<%--
 * header.jsp
 *
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>

<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags"%>

<div>
	<img src="images/logo.png" alt="Acme-Antenna Co., Inc." />
	<img src="${bannerX }" id="banner-custom" height="130">
</div>

<div>
	<ul id="jMenu">
		<!-- Do not forget the "fNiv" class for the first level links !! -->
		<security:authorize access="hasRole('ADMIN')">
			<li><a class="fNiv"><spring:message	code="master.page.administrator" /></a>
				<ul>
					<li class="arrow"></li>
					<li><a href="administrator/dashboard.do"><spring:message code="master.page.administrator.dashboard" /></a></li>
					<li><a href="administrator/actorList.do"><spring:message code="master.page.administrator.ban" /></a></li>
					<li><a href="tabooWord/administrator/list.do"><spring:message code="master.page.tabooWord.administrator.list" /></a></li>
					<li><a href="tutorial/administrator/list.do"><spring:message code="master.page.tutorial.administrator.list" /></a></li>
					<li><a href="comment/administrator/list.do"><spring:message code="master.page.comment.administrator.list" /></a></li>
					<li><a href="administrator/utilsView.do"><spring:message code="master.page.administrator.utils"/></a></li>
				</ul>
			</li>
			<li><a class="fNiv"><spring:message	code="master.page.banner" /></a>
				<ul>
					<li><a href="banner/list.do"><spring:message code="master.page.banner.list" /></a></li>
				</ul>
			</li>	
		</security:authorize>
		
		<security:authorize access="hasRole('USER')">

			<li><a class="fNiv"><spring:message	code="master.page.user" /></a>
				<ul>
					<li class="arrow"></li>
					<li><a href="antenna/user/list.do"><spring:message code="master.page.antenna.user.list" /></a></li>
					<li><a href="antenna/user/create.do"><spring:message code="master.page.antenna.user.create" /></a></li>		
					<li><a href="subscription/user/list.do"><spring:message code="master.page.subscription.user.list" /></a></li>
					<li><a href="request/user/alreadyList.do"><spring:message code="master.page.request.user.alreadyList" /></a></li>
					<li><a href="request/user/noYetList.do"><spring:message code="master.page.request.user.noYetList" /></a></li>
				</ul>
			</li>

		</security:authorize>
		
		<security:authorize access="hasRole('AGENT')">
			<li><a class="fNiv"><spring:message	code="master.page.banner" /></a>
				<ul>
					<li><a href="banner/create.do"><spring:message code="master.page.banner.register" /></a></li>
					<li><a href="banner/myList.do"><spring:message code="master.page.banner.myList" /></a></li>	
				</ul>
			</li>	
			
			
		</security:authorize>
		
				<security:authorize access="hasRole('HANDYWORKER')">
			<li><a class="fNiv"><spring:message	code="master.page.handyworker" /></a>
				<ul>
					<li><a href="request/handyworker/alreadyList.do"><spring:message code="master.page.request.handyworker.alreadyList" /></a></li>
					<li><a href="request/handyworker/noYetList.do"><spring:message code="master.page.request.handyworker.noYetList" /></a></li>	
				</ul>
			</li>	
			
			
		</security:authorize>
		
		<security:authorize access="isAnonymous()">
			<li><a class="fNiv" href="security/login.do"><spring:message code="master.page.login" /></a></li>
			<li><a href="user/create.do"><spring:message code="master.page.user.register" /></a></li>
			<li><a href="handyworker/create.do"><spring:message code="master.page.handyworker.register" /></a></li>
			<li><a href="agent/create.do"><spring:message code="master.page.agent.register" /></a></li>
			<li><a href="satellite/list.do"><spring:message code="master.page.satellite.list" /></a></li>
			<li><a href="platform/list.do"><spring:message code="master.page.platform.list" /></a></li>
			<li><a href="tutorial/list2.do"><spring:message code="master.page.tutorial.list" /></a></li>
			<li><a href="handyworker/list.do"><spring:message code="master.page.handyworker.list" /></a></li>

		</security:authorize>
		
		<security:authorize access="isAuthenticated()">
			<li><a href="satellite/list.do"><spring:message code="master.page.satellite.list" /></a></li>
			<li><a href="platform/list.do"><spring:message code="master.page.platform.list" /></a></li>
			<li><a href="tutorial/list.do"><spring:message code="master.page.tutorial.list" /></a></li>
			<li><a href="handyworker/list.do"><spring:message code="master.page.handyworker.list" /></a></li>
			<li>
				<a class="fNiv"> 
					<spring:message code="master.page.profile" /> 
			        (<security:authentication property="principal.username" />)
				</a>
				<ul>
					<li class="arrow"></li>
					<security:authorize access="hasRole('USER')">
						<!-- <li><a href="user/edit.do"><spring:message code="master.page.user.edit" /></a></li> -->
					</security:authorize>
					<security:authorize access="hasRole('HANDYWORKER')">
						<!-- <li><a href="handyworker/edit.do"><spring:message code="master.page.handyworker.edit" /></a></li> -->
					</security:authorize>					
					<li><a href="j_spring_security_logout"><spring:message code="master.page.logout" /> </a></li>
				</ul>
			</li>
		</security:authorize>
	</ul>
</div>

<div>
	<a href="?language=en">en</a> | <a href="?language=es">es</a>
</div>

