<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>


<%--
  ~ Copyright  2017. All information contained here included the intellectual and technical concepts are property of Null Point Software.
  --%>
<!-- Listing grid -->
<display:table pagesize="15" class="displaytag" keepStatus="true"
	name="requests" requestURI="${requestURI}" id="row">
	<c:set var="localeCode" value="${pageContext.response.locale}" />

	<security:authorize access="hasAnyRole('USER','HANDYWORKER')">
		<display:column>
			<a href="request/edit.do?requestId=${row.id}"> <spring:message
					code="request.edit" />
			</a>
		</display:column>
	</security:authorize>
	<security:authorize access="hasRole('HANDYWORKER')">
		<display:column>
			<jstl:if test="${row.isAttended == false}">
				<a href="request/handyworker/serviceRequest.do?requestId=${row.id}">
					<spring:message code="request.serviceRequest" />
				</a>

			</jstl:if>
		</display:column>
	</security:authorize>
	<security:authorize access="hasRole('HANDYWORKER')">
		<display:column>
			<jstl:if test="${row.isAttended == true}">
				<a href="request/handyworker/serviceDone.do?requestId=${row.id}">
					<spring:message code="request.serviceDone" />
				</a>

			</jstl:if>
		</display:column>
	</security:authorize>
	<spring:message code="request.moment" var="moment" />
	<display:column property="moment" title="${moment}" sortable="true" />
	<spring:message code="request.description" var="description" />
	<display:column property="description" title="${description}"
		sortable="true" />


	<spring:message code="request.serviceDone" var="serviceDone" />
	<c:choose>
		<c:when test="${localeCode == 'en'}">
			<display:column property="serviceDone" title="${serviceDone}" sortable="true"
				format="{0,date,yyyy/MM/dd}" />
		</c:when>
		<c:when test="${localeCode == 'es'}">
			<display:column property="serviceDone" title="${serviceDone}" sortable="true"
				format="{0,date,dd-MM-yyyy}" />
		</c:when>
	</c:choose>

	<spring:message code="request.descriptionResult"
		var="descriptionResult" />
	<display:column property="descriptionResult"
		title="${descriptionResult}" sortable="true" />
	<spring:message code="request.creditCard" var="creditCard" />
	<security:authorize access="hasRole('USER')">
		<display:column property="creditCard.holderName" title="${holderName}"
			sortable="true" />
		<display:column property="creditCard.brandName" title="${brandName}"
			sortable="true" />
		<display:column property="creditCard.number" title="${number}"
			sortable="true" />
		<display:column property="creditCard.expirationYear"
			title="${expirationYear}" sortable="true" />
		<display:column property="creditCard.expirationMonth"
			title="${expirationMonth}" sortable="true" />
		<display:column property="creditCard.CVV" title="${CVV}"
			sortable="true" />
	</security:authorize>
</display:table>