<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>


<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@ taglib prefix="acme" tagdir="/WEB-INF/tags"%>

<form:form action="request/edit.do" modelAttribute="request">

	<form:hidden path="id" />
	<form:hidden path="version" />
	<form:hidden path="moment" />
	<form:hidden path="serviceDone" />
	<form:hidden path="user" />
	<form:hidden path="handyworker" />
	<form:hidden path="isAttended" />

	<security:authorize access="hasRole('USER')">
	<acme:textbox path="description" code="request.description" />
	</security:authorize>
	<!-- 
		<form:label path="isAttended">
		<spring:message code="request.isAttended" />
	</form:label>
	<security:authorize access="hasRole('HANDYWORKER')">
	<form:select id="isAttended" path="isAttended">
		<form:option value="0" label="false" />
		<form:option value="1" label="true" />
	</form:select>
	<form:errors path="isAttended" cssClass="error" />
	</security:authorize>
	<br />
	-->
	<security:authorize access="hasRole('HANDYWORKER')">
	<acme:textbox path="descriptionResult" code="request.descriptionResult" />
	<br />
	</security:authorize>
	<security:authorize access="hasRole('USER')">
	<acme:textbox path="creditCard.holderName"
		code="request.creditCard.holderName" value="${cookie.holderCookie.value }"/>
	
	<acme:textbox path="creditCard.brandName"
		code="request.creditCard.brandName" value="${cookie.brandCookie.value }"/>
	
	<acme:textbox path="creditCard.number" code="request.creditCard.number" value="${cookie.numberCookie.value }"/>
	
	<acme:textbox path="creditCard.expirationYear"
		code="request.creditCard.expirationYear" value="${cookie.yearCookie.value }"/>
	
	<acme:textbox path="creditCard.expirationMonth"
		code="request.creditCard.expirationMonth" value="${cookie.monthCookie.value }"/>
	
	<acme:textbox path="creditCard.CVV" code="request.creditCard.CVV" value="${cookie.cvvCookie.value }"/>
	
	<br />

	</security:authorize>
	<!---------------------------- BOTONES -------------------------->



	<input type="submit" name="save"
		value="<spring:message code="request.save" />" />&nbsp;
           

    <input type="button" name="cancel"
		value="<spring:message code="request.cancel" />"
		onclick="javascript: window.location.replace('request/list.do')" />

</form:form>