<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>

<!-- Listing grid -->
<display:table pagesize="5" class="displaytag" keepStatus="true"
	name="banners" requestURI="${requestURI}" id="row">
	
	<security:authorize access="hasRole('AGENT')">
		<display:column>
			<a href="banner/edit.do?bannerId=${row.id}"> <spring:message
					code="banner.edit" />
			</a>
		</display:column>
	</security:authorize>
	
	<security:authorize access="hasRole('ADMIN')">
		<display:column>
			<a href="banner/delete.do?bannerId=${row.id}" onclick="return confirm('<spring:message code="banner.confirm.delete"/>') "> 
			<spring:message code="banner.delete" />
			</a>
		</display:column>
	</security:authorize>
	
	<spring:message code="banner.picture" var="picture" />
	<display:column property="picture" title="${picture}" sortable="true" />
	<spring:message code="banner.targetPage" var="targetPage" />
	<display:column property="targetPage" title="${targetPage}" sortable="true" />
	<spring:message code="banner.creditCard" var="creditCard" />
	<display:column property="creditCard.holderName" title="${holderName}" sortable="true" />
	<display:column property="creditCard.brandName" title="${brandName}" sortable="true" />
	<display:column property="creditCard.number" title="${number}" sortable="true" />
	<display:column property="creditCard.expirationYear" title="${expirationYear}" sortable="true" />
	<display:column property="creditCard.expirationMonth" title="${expirationMonth}" sortable="true" />
	<display:column property="creditCard.CVV" title="${CVV}" sortable="true" />
	
</display:table>