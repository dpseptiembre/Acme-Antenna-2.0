<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>


<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@ taglib prefix="acme" tagdir="/WEB-INF/tags"%>


<form:form action="banner/edit.do" modelAttribute="banner">

	<form:hidden path="id" />
	<form:hidden path="version" />
	<form:hidden path="agent"/>
	
	<acme:textbox path="picture" code="banner.picture"/>
	<br />
	<acme:textbox path="targetPage" code="banner.targetPage"/>
	<br />
	<acme:textbox path="creditCard.holderName" code="banner.creditCard.holderName"/>
	<acme:textbox path="creditCard.brandName" code="banner.creditCard.brandName"/>
	<acme:textbox path="creditCard.number" code="banner.creditCard.number"/>
	<acme:textbox path="creditCard.expirationYear" code="banner.creditCard.expirationYear"/>
	<acme:textbox path="creditCard.expirationMonth" code="banner.creditCard.expirationMonth"/>
	<acme:textbox path="creditCard.CVV" code="banner.creditCard.CVV"/>
	<br />
	
			<!---------------------------- BOTONES -------------------------->



    <input type="submit" name="save"
           value="<spring:message code="banner.save" />" 
           />&nbsp;
           
	<security:authorize access="hasRole('ADMIN')">
		<jstl:if test="${banner.id != 0}">
		<input type="submit" name="delete"
				value="<spring:message code="banner.delete" />"
				onclick="return confirm('<spring:message code="banner.confirm.delete" />')" />&nbsp;
		</jstl:if>
	
	</security:authorize>
    <security:authorize access="hasRole('AGENT')">
    <input type="button" name="cancel"
           value="<spring:message code="banner.cancel" />"
           onclick="javascript: window.location.replace('banner/myList.do')" />
	</security:authorize>
	<security:authorize access="hasRole('ADMIN')">
	<input type="button" name="cancel"
           value="<spring:message code="banner.cancel" />"
           onclick="javascript: window.location.replace('banner/list.do')" />
	</security:authorize>
	
</form:form>