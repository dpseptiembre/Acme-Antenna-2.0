<%@page language="java" contentType="text/html; charset=ISO-8859-1"
        pageEncoding="ISO-8859-1" %>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@taglib prefix="security"
          uri="http://www.springframework.org/security/tags" %>
<%@taglib prefix="display" uri="http://displaytag.sf.net" %>
<%@ taglib prefix="acme" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%--
  ~ Copyright � 2017. All information contained here included the intellectual and technical concepts are property of Null Point Software.
  --%>


<div class="container">

    <h2><spring:message code="admin.sysemCustom"/></h2>
    <form:form action="administrator/customize.do" method="get">
        <p><spring:message code="admin.systemName"/></p>
        <input value="${systemName}" name="systemName" class="form-group" type="text" title="SystemName" size="30"/>
        <br>
        <p><spring:message code="admin.logo"/></p>
        <input value="${logo}" name="logo" class="form-group" type="text" size="65"/>
        <br>
        <p><spring:message code="admin.welcome"/></p>
        <input value="${welcome}" name="welcome" class="form-group" type="text" size="65"/>
        <br>
        <p><spring:message code="admin.welcomeES"/></p>
        <input value="${welcomeES}" name="welcomeES" class="form-group" type="text" size="65"/>
        <br>
        <input type="submit" value="<spring:message code="admin.save" />" class="btn btn-primary"/>
        <input type="button" name="cancel"
		value="<spring:message code="general.cancel" />"
		onclick="javascript: window.location.replace('/Acme-Antenna/')" />
    </form:form>

</div>