<%@page language="java" contentType="text/html; charset=ISO-8859-1"
        pageEncoding="ISO-8859-1" %>
<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@taglib prefix="security"
          uri="http://www.springframework.org/security/tags" %>
<%@taglib prefix="display" uri="http://displaytag.sf.net" %>
<%@ taglib prefix="acme" tagdir="/WEB-INF/tags" %>


<%--
  ~ Copyright  2017. All information contained here included the intellectual and technical concepts are property of Null Point Software.
  --%>
<div class="container">
<form:form action="handyworker/register.do" modelAttribute="handyworker">

    <form:hidden path="id"/>
    <form:hidden path="version"/>
    <form:hidden path="ban"/>
    <form:hidden path="userAccount.authorities"/>

    <acme:textbox path="name" code="handyworker.name"/>
    <acme:textbox path="surname" code="handyworker.surname"/>
    <acme:textbox path="picture" code="handyworker.picture"/>
    <acme:textbox path="email" code="handyworker.email"/>
    <acme:textbox path="phone" code="handyworker.phone"/>
    <acme:textbox path="postalAddress" code="handyworker.postalAddress"/>
    <acme:textbox path="antennaModel" code="handyworker.antennaModel"/>

    <br/>
  <h1>User Account</h1>
    <br>
    <form:label path="userAccount.username">
        <spring:message code="handyworker.username"/>:
    </form:label>
    <form:input path="userAccount.username"/>
    <form:errors cssClass="error" path="userAccount.username"/>
    <br/>
    <br>
    <form:label path="userAccount.password">
        <spring:message code="handyworker.password"/>:
    </form:label>
    <form:password path="userAccount.password"/>
    <form:errors cssClass="error" path="userAccount.password"/>
    <br/>
    <br/>

    <!---------------------------- BOTONES -------------------------->
    <input class="btn btn-info" type="submit" name="save"
           value="<spring:message code="handyworker.save" />"/>
    <a class="btn btn-danger" href="welcome/index.do"><spring:message code="general.cancel"/></a>


</form:form>
</div>