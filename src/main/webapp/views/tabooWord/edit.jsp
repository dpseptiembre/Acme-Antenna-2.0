<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Date"%>
<%@page language="java" contentType="text/html; charset=ISO-8859-1"
        pageEncoding="ISO-8859-1" %>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags" %>
<%@taglib prefix="display" uri="http://displaytag.sf.net" %>
<%@ taglib prefix="acme" tagdir="/WEB-INF/tags" %>



<form:form action="tabooWord/administrator/edit.do" modelAttribute="tabooWord">

    <form:hidden path="id"/>
    <form:hidden path="version"/>
    
    <acme:textbox path="word" code="tabooWord.word"/>
    <br />
    
    
        <!---------------------------- BOTONES -------------------------->



    <input type="submit" name="save"
           value="<spring:message code="tabooWord.save" />" />


    <input type="button" name="cancel"
           value="<spring:message code="tabooWord.save.cancel" />"
           onclick="javascript: window.location.replace('tabooWord.save/administrator/list.do')" />

</form:form>