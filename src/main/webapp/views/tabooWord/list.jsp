<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@ taglib prefix="acme" tagdir="/WEB-INF/tags" %>


<security:authorize access="hasRole('ADMIN')">
	<div>
		<H5>
			<a href="tabooWord/administrator/create.do"> <spring:message
					code="tabooWord.create" />
			</a>
		</H5>
	</div>
</security:authorize>
<!-- Listing grid -->
<display:table pagesize="5" class="displaytag" keepStatus="true"
	name="tabooWords" requestURI="${requestURI}" id="row">
	
	<acme:column code="tabooWord.word" property="word" sortable="true"/>
	
	<security:authorize access="hasRole('ADMIN')">
		<display:column>
				<a href="tabooWord/administrator/delete.do?tabooWordId=${row.id}"> <spring:message
						code="tabooWord.delete" />
				</a>
		</display:column>
		</security:authorize>
</display:table>	