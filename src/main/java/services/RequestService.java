
package services;

import java.util.Collection;
import java.util.Date;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import repositories.RequestRepository;
import domain.Handyworker;
import domain.Request;

@Service
@Transactional
public class RequestService {

	@Autowired
	private RequestRepository	requestRepository;


	public RequestService() {
		super();
	}

	public Request create() {
		final Request res = new Request();
		res.setIsAttended(false);
		return res;
	}

	public Request findOne(final int requestId) {
		Request result;

		result = this.requestRepository.findOne(requestId);

		return result;
	}

	public Collection<Request> findAll() {
		Collection<Request> result;

		result = this.requestRepository.findAll();

		return result;
	}

	public Request save(final Request Request) {
		Assert.notNull(Request);
		return this.requestRepository.save(Request);
	}

	public void delete(final Request Request) {
		Assert.notNull(Request);
		Assert.isTrue(this.requestRepository.exists(Request.getId()));
		this.requestRepository.delete(Request);
	}

	public void deleteRequestAntenna(final Integer antennaId) {
		Collection<Request> requests;
		requests = this.requestRepository.findRequestAntenna(antennaId);
		for (final Request r : requests)
			this.delete(r);

	}

	public Collection<Request> findByUserId(final int userId) {
		Collection<Request> result;
		result = this.requestRepository.findByUserId(userId);
		return result;
	}

	public Collection<Request> findByHandyworkerId(final int handyworkerId) {
		Collection<Request> result;
		result = this.requestRepository.findByHandyworkerId(handyworkerId);
		return result;
	}

	public Collection<Request> findAlreadyRequestByUserId(int userId) {
		Collection<Request> result;
		result = this.requestRepository.findAlreadyRequestByUserId(userId);
		return result;
	}
	
	public Collection<Request> findNoYetRequestByUserId(int userId) {
		Collection<Request> result;
		result = this.requestRepository.findNoYetRequestByUserId(userId);
		return result;
	}
	
	public Collection<Request> findAlreadyRequestByHandyworkerId(int handyworkerId) {
		Collection<Request> result;
		result = this.requestRepository.findAlreadyRequestByHandyworkerId(handyworkerId);
		return result;
	}
	
	public Collection<Request> findNoYetRequestByHandyworkerId(int handyworkerId) {
		Collection<Request> result;
		result = this.requestRepository.findNoYetRequestByHandyworkerId(handyworkerId);
		return result;
	}

	public void serviceRequest(Request request) {
		request.setIsAttended(true);
		requestRepository.save(request);
	}

	public void serviceDone(Request request) {
		Date date;
		date=new Date();
		request.setServiceDone(date);
		requestRepository.save(request);
	}
	
}
