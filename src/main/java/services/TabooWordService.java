package services;

import java.util.Collection;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import repositories.TabooWordRepository;

import domain.TabooWord;

@Service
@Transactional
public class TabooWordService {

	@Autowired
	private TabooWordRepository tabooWordRepository;

	public TabooWordService() {
		super();
	}

	public TabooWord create() {
		final TabooWord res;
		res = new TabooWord();
		return res;

	}

	public TabooWord findOne(final int tabooWordId) {
		Assert.isTrue(tabooWordId != 0);
		TabooWord result;
		result = this.tabooWordRepository.findOne(tabooWordId);
		Assert.notNull(result);

		return result;

	}

	public Collection<TabooWord> findAll() {
		Collection<TabooWord> result;

		result = this.tabooWordRepository.findAll();
		Assert.notNull(result);

		return result;

	}

	public TabooWord save(final TabooWord tabooWord) {
		Assert.notNull(tabooWord);

		TabooWord result;

		result = this.tabooWordRepository.save(tabooWord);

		return result;

	}

	public void delete(final TabooWord tabooWord) {
		Assert.notNull(tabooWord);
		Assert.isTrue(tabooWord.getId() != 0);
		Assert.isTrue(this.tabooWordRepository.exists(tabooWord.getId()));

		this.tabooWordRepository.delete(tabooWord);

	}

}
