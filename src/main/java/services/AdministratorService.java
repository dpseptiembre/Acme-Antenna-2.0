package services;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Random;
import java.util.Set;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import repositories.AdministratorRepository;
import repositories.AntennaRepository;

import com.google.gson.Gson;

import domain.Actor;
import domain.Administrator;
import domain.Agent;
import domain.Antenna;
import domain.Banner;
import domain.Uttils;

@Service
@Transactional
public class AdministratorService {

	@Autowired
	private AdministratorRepository administratorRepository;

	@Autowired
	private AntennaRepository antennaRepository;

	@Autowired
	private ActorService actorService;

	@Autowired
	private AgentService agentService;

	@Autowired
	private UttilsService uttilsService;
	
	@Autowired
	private BannerService bannerService;
	

	public AdministratorService() {
		super();
	}

	public Administrator create() {
		return new Administrator();
	}

	public Administrator findOne(final int administratorId) {
		Administrator result;

		result = this.administratorRepository.findOne(administratorId);

		return result;
	}

	public Collection<Administrator> findAll() {
		Collection<Administrator> result;

		result = this.administratorRepository.findAll();

		return result;
	}

	public Administrator save(final Administrator administrator) {
		Assert.notNull(administrator);
		return this.administratorRepository.save(administrator);
	}

	public void delete(final Administrator administrator) {
		Assert.notNull(administrator);
		Assert.isTrue(this.administratorRepository.exists(administrator.getId()));
		this.administratorRepository.delete(administrator);
	}

	public Double avgAntennaPerUser() {
		Double result;

		result = this.administratorRepository.averageNumberOfAntennasPerUser();

		return result;
	}

	public Double standarDesviationOfAntennasPerUser() {
		Double result;
		result = this.administratorRepository
				.standarDesviationOfAntennasPerUser();
		return result;
	}

	public Double averageQualityOfAntennas() {
		Double result;
		result = this.administratorRepository.averageQualityOfAntennas();
		return result;
	}

	public Double standarDesviationQualityOfAntennas() {
		Double result;
		result = this.administratorRepository
				.standarDesviationQualityOfAntennas();
		return result;
	}

	public Collection<String> top3ModelsOfAntennas() {
		Collection<String> result;
		result = this.administratorRepository.top3ModelsOfAntennas(
				new PageRequest(0, 3)).getContent();
		return result;
	}

	public Collection<String> models() {
		Collection<String> res;
		return res = this.administratorRepository.models();
	}

	public String createChart() {
		final Gson gsonObj = new Gson();
		Map<Object, Object> map = null;
		final List<Map<Object, Object>> list = new ArrayList<>();

		final Set<Antenna> antennas = new HashSet<>(
				this.antennaRepository.findAll());
		final Set<String> models = new HashSet<>(this.models());
		int count = 0;
		for (final String s : models) {
			for (final Antenna a : antennas) {
				map = new HashMap<Object, Object>();
				map.put("label", s);
				if (a.getModel().equals(s))
					count++;
				map.put("y", count);
			}
			list.add(map);
			count = 0;
		}
		final String dataPoints = gsonObj.toJson(list);
		return dataPoints;
	}

	public Double averageNumberOfTutorialsPerUser() {
		Double result;
		result = this.administratorRepository.averageNumberOfTutorialsPerUser();
		return result;
	}

	public Double standarDesviationOfTutorialsPerUser() {
		Double result;
		result = this.administratorRepository
				.standarDesviationOfTutorialsPerUser();
		return result;
	}

	public Double averageNumberOfCommentsPerTutorial() {
		Double result;
		result = this.administratorRepository
				.averageNumberOfCommentsPerTutorial();
		return result;
	}

	public Double standarDesviationOfCommentsPerTutorial() {
		Double result;
		result = this.administratorRepository
				.standarDesviationOfCommentsPerTutorial();
		return result;
	}

	public Collection<Actor> actorsPublishedTutorialAboveAVG() {
		Collection<Actor> result;
		result = this.administratorRepository.actorsPublishedTutorialAboveAVG();
		return result;
	}

	public Double averageNumberOfRepliesPerComment() {
		Double result;
		result = this.administratorRepository
				.averageNumberOfRepliesPerComment();
		return result;
	}

	public Double standarDesviationOfRepliesPerComment() {
		Double result;
		result = this.administratorRepository
				.standarDesviationOfRepliesPerComment();
		return result;
	}

	public Double averageNumberOfLengthOfComment() {
		Double result;
		result = this.administratorRepository.averageNumberOfLengthOfComment();
		return result;
	}

	public Double standarDesviationOfLengthOfComment() {
		Double result;
		result = this.administratorRepository
				.standarDesviationOfLengthOfComment();
		return result;
	}

	public Double averageNumberOfPicturesPerTutorial() {
		Double result;
		result = this.administratorRepository
				.averageNumberOfPicturesPerTutorial();
		return result;
	}

	public Double standarDesviationOfPicturesPeroTutorial() {
		Double result;
		result = this.administratorRepository
				.standarDesviationOfPicturesPeroTutorial();
		return result;
	}

	public Double averageNumberOfPicturesPerComment() {
		Double result;
		result = this.administratorRepository
				.averageNumberOfPicturesPerComment();
		return result;
	}

	public Double standarDesviationOfPicturesPeroComment() {
		Double result;
		result = this.administratorRepository
				.standarDesviationOfPicturesPeroComment();
		return result;
	}

	public void ban(final Actor actor) {
		actor.setBan(true);
		actor.getUserAccount().setIsAccountNonLocked(false);
		this.actorService.save(actor);

	}

	public void unban(final Actor actor) {
		actor.setBan(false);
		actor.getUserAccount().setIsAccountNonLocked(true);
		this.actorService.save(actor);

	}

	public Double averageNumberOfRequestsPerUser() {
		final Double result = this.administratorRepository
				.averageNumberOfRequestsPerUser();
		return result;
	}

	public Double standarDesviationOfRequestsPerUser() {
		return this.administratorRepository
				.standarDesviationOfRequestsPerUser();
	}

	public Double averageServicedRequestPerUser() {
		Double res = this.administratorRepository
				.averageServicedRequestPerUser();
		if (res == null)
			res = 0.0;
		return res;
	}

	public Double averageServicedRequestPerHandyworker() {
		Double res = this.administratorRepository
				.averageServicedRequestPerHandyworker();
		if (res == null)
			res = 0.0;
		return res;
	}

	public Double averageNumberOfBannersPerAgent() {
		return this.administratorRepository.averageNumberOfBannersPerAgent();
	}

	public Collection<Agent> top3AgentsForBanners() {
		final Collection<Agent> agents = this.agentService.findAll();
		final List<Agent> aux = new ArrayList<Agent>();
		Integer max = 0;
		for (final Agent a : agents)
			if (a.getBanners().size() >= max) {
				max = a.getBanners().size();
				aux.add(a);
			}
		final List<Agent> res = new ArrayList<Agent>();
		if (aux.size() == 1)
			res.addAll(aux);
		else if (aux.size() == 2)
			res.addAll(aux);
		else
			for (int i = aux.size(); i <= aux.size() - 3; i--)
				res.add(aux.get(i));
		return res;
	}
	
	public Uttils getUttils(){
	      try {
	         List<Uttils> uttils = new ArrayList<>(uttilsService.findAll());
	         if (uttils.isEmpty()){
	            throw new NoSuchElementException("The util list is empty");
	         }else {
	            Uttils first = uttils.get(0);
	            return first;
	         }

	      } catch (Exception e) {
	         e.printStackTrace();
	         return null;
	      }
	   }
	
	  public void customize(String systemName, String logo, String welcome, String welcomeES){

	      try {
	         getUttils().setLogo(logo);
	         getUttils().setSystemName(systemName);
	         getUttils().setWelcomeMezzage(welcome);
	         getUttils().setWelcomeMezzageES(welcomeES);
	         uttilsService.save(getUttils());
	      } catch (Exception e) {
	         e.printStackTrace();
	      }
	   }

	public String getRamdomBanner() {
		
		String result;
		result=bannerService.getRamdomBanner();
		
		return result;
	}

}
