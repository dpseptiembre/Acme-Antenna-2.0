
package services;

import java.util.ArrayList;
import java.util.Collection;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.encoding.Md5PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import repositories.AgentRepository;
import security.Authority;
import security.LoginService;
import security.UserAccount;
import security.UserAccountService;
import domain.Actor;
import domain.Agent;

@Service
@Transactional
public class AgentService {

	@Autowired
	private AgentRepository		agentRepository;

	@Autowired
	private UserAccountService	userAccountService;


	public AgentService() {
		super();
	}

	public Agent create() {
		final Agent res = new Agent();
		this.authoritySet(res);
		return res;

	}

	public Agent findOne(final int agentId) {
		Assert.isTrue(agentId != 0);
		Agent result;
		result = this.agentRepository.findOne(agentId);
		Assert.notNull(result);

		return result;

	}

	public Collection<Agent> findAll() {
		Collection<Agent> result;

		result = this.agentRepository.findAll();
		Assert.notNull(result);

		return result;

	}

	public Agent save(final Agent agent) {
		Assert.notNull(agent);

		Agent result;

		result = this.agentRepository.save(agent);

		return result;

	}

	public void delete(final Agent agent) {
		Assert.notNull(agent);
		Assert.isTrue(agent.getId() != 0);
		Assert.isTrue(this.agentRepository.exists(agent.getId()));

		this.agentRepository.delete(agent);

	}

	// Other business methods -------------------------------------------------

	public UserAccount findUserAccount(final Agent agent) {
		Assert.notNull(agent);

		UserAccount result;

		result = this.userAccountService.findByAgent(agent);

		return result;
	}

	public Agent findByPrincipal() {
		Agent result;
		UserAccount userAccount;
		userAccount = LoginService.getPrincipal();
		Assert.notNull(userAccount);
		result = this.findByUserAccount(userAccount);
		Assert.notNull(result);
		return result;
	}

	public Agent findByUserAccount(final UserAccount userAccount) {
		Assert.notNull(userAccount);

		Agent result;

		result = this.agentRepository.findByUserAccountId(userAccount.getId());

		return result;
	}

	public Actor registerAsAgent(final Agent u) {
		Assert.notNull(u);
		u.getUserAccount().setUsername(u.getUserAccount().getUsername());
		Md5PasswordEncoder encoder;
		encoder = new Md5PasswordEncoder();
		final String hash = encoder.encodePassword(u.getUserAccount().getPassword(), null);
		u.getUserAccount().setPassword(hash);
		final UserAccount userAccount = this.userAccountService.save(u.getUserAccount());
		u.setUserAccount(userAccount);
		Assert.notNull(u.getUserAccount().getAuthorities(), "authorities null al registrar");
		u.setBan(false);
		u.getUserAccount().setIsAccountNonLocked(true);
		final Agent resu = this.agentRepository.save(u);
		return resu;
	}

	private void authoritySet(final Agent agent) {
		Assert.notNull(agent);
		final Authority autoh = new Authority();
		autoh.setAuthority(Authority.AGENT);
		final Collection<Authority> authorities = new ArrayList<>();
		authorities.add(autoh);
		final UserAccount res1 = new UserAccount();
		res1.setAuthorities(authorities);
		final UserAccount userAccount = this.userAccountService.save(res1);
		agent.setUserAccount(userAccount);
		Assert.notNull(agent.getUserAccount().getAuthorities(), "authorities null al registrar");
	}

}
