
package services;

import java.util.Collection;
import java.util.List;
import java.util.Random;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import repositories.BannerRepository;
import domain.Agent;
import domain.Banner;

@Service
@Transactional
public class BannerService {

	@Autowired
	private BannerRepository		bannerRepository;

	@Autowired
	private static BannerRepository	bannerRepository2;

	@Autowired
	private static AgentService	agentService;

	public BannerService() {
		super();
	}

	public Banner create() {
		return new Banner();
	}

	public Banner findOne(final int BannerId) {
		Banner result;

		result = this.bannerRepository.findOne(BannerId);

		return result;
	}

	public Collection<Banner> findAll() {
		Collection<Banner> result;

		result = this.bannerRepository.findAll();

		return result;
	}

	public Banner save(final Banner Banner) {
		Assert.notNull(Banner);
		return this.bannerRepository.save(Banner);
	}

	public void delete(final Banner Banner) {
		Assert.notNull(Banner);
		Assert.isTrue(this.bannerRepository.exists(Banner.getId()));
		this.bannerRepository.delete(Banner);
	}

	public String getRamdomBanner() {
		Collection<Banner> banners;
		List<Banner> b;
		final Random rand = new Random();
		rand.setSeed(System.currentTimeMillis());
		banners = this.bannerRepository.findAll();
		b = (List<Banner>) banners;
		final int i = rand.nextInt(banners.size());

		String result;
		result = b.get(i).getPicture();

		return result;
	}

	public static String getRamdomBanner2() {
		Collection<Banner> banners;
		List<Banner> b;
		final Random rand = new Random();
		rand.setSeed(System.currentTimeMillis());
		banners = BannerService.bannerRepository2.findAll();
		b = (List<Banner>) banners;
		final int i = rand.nextInt(banners.size());

		String result;
		result = b.get(i).getPicture();
		return result;
	}

	public Collection<Banner> findAgentBanner(int agentId) {
		Collection<Banner> result;
		result=bannerRepository.findAgentBanner(agentId);
		
		return result;
	}
}
