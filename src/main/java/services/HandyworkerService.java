
package services;

import java.util.ArrayList;
import java.util.Collection;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.encoding.Md5PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import repositories.HandyworkerRepository;
import security.Authority;
import security.LoginService;
import security.UserAccount;
import security.UserAccountService;
import domain.Actor;
import domain.Handyworker;

@Service
@Transactional
public class HandyworkerService {

	@Autowired
	private HandyworkerRepository	handyworkerRepository;

	@Autowired
	private UserAccountService		userAccountService;


	public HandyworkerService() {
		super();
	}

	public Handyworker create() {
		final Handyworker res = new Handyworker();
		this.authoritySet(res);
		return res;
	}

	public Handyworker findOne(final int handyworkerId) {
		Assert.isTrue(handyworkerId != 0);
		Handyworker result;
		result = this.handyworkerRepository.findOne(handyworkerId);
		Assert.notNull(result);

		return result;
	}

	public Collection<Handyworker> findAll() {
		Collection<Handyworker> result;

		result = this.handyworkerRepository.findAll();
		Assert.notNull(result);

		return result;

	}

	public Handyworker save(final Handyworker handyworker) {
		Assert.notNull(handyworker);

		Handyworker result;

		result = this.handyworkerRepository.save(handyworker);

		return result;

	}

	public void delete(final Handyworker handyworker) {
		Assert.notNull(handyworker);
		Assert.isTrue(handyworker.getId() != 0);
		Assert.isTrue(this.handyworkerRepository.exists(handyworker.getId()));

		this.handyworkerRepository.delete(handyworker);

	}

	public UserAccount findUserAccount(final Handyworker handyworker) {
		Assert.notNull(handyworker);

		UserAccount result;

		result = this.userAccountService.findByHandyworker(handyworker);

		return result;
	}

	public Handyworker findByPrincipal() {
		Handyworker result;
		UserAccount userAccount;
		userAccount = LoginService.getPrincipal();
		Assert.notNull(userAccount);
		result = this.findByUserAccount(userAccount);
		Assert.notNull(result);
		return result;
	}

	public Handyworker findByUserAccount(final UserAccount userAccount) {
		Assert.notNull(userAccount);

		Handyworker result;

		result = this.handyworkerRepository.findByUserAccountId(userAccount.getId());

		return result;
	}

	public Actor registerAsHandyworker(final Handyworker u) {
		Assert.notNull(u);
		u.getUserAccount().setUsername(u.getUserAccount().getUsername());
		Md5PasswordEncoder encoder;
		encoder = new Md5PasswordEncoder();
		final String hash = encoder.encodePassword(u.getUserAccount().getPassword(), null);
		u.getUserAccount().setPassword(hash);
		final UserAccount userAccount = this.userAccountService.save(u.getUserAccount());
		u.setUserAccount(userAccount);
		Assert.notNull(u.getUserAccount().getAuthorities(), "authorities null al registrar");
		u.setBan(false);
		u.getUserAccount().setIsAccountNonLocked(true);
		final Handyworker resu = this.handyworkerRepository.save(u);
		return resu;
	}

	private void authoritySet(final Handyworker handyworker) {
		Assert.notNull(handyworker);
		final Authority autoh = new Authority();
		autoh.setAuthority(Authority.HANDYWORKER);
		final Collection<Authority> authorities = new ArrayList<>();
		authorities.add(autoh);
		final UserAccount res1 = new UserAccount();
		res1.setAuthorities(authorities);
		final UserAccount userAccount = this.userAccountService.save(res1);
		handyworker.setUserAccount(userAccount);
		Assert.notNull(handyworker.getUserAccount().getAuthorities(), "authorities null al registrar");
	}

}
