package services;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import repositories.UttilsRepository;
import domain.Uttils;

@Service
@Transactional
public class UttilsService {

   // Managed repository -----------------------------------------------------

   @Autowired
   private UttilsRepository uttilsRepository;

   // Supporting services ----------------------------------------------------


   // Constructors -----------------------------------------------------------

   public UttilsService() {
      super();
   }

   // Simple CRUD methods ----------------------------------------------------

   public Collection<Uttils> findAll() {
      Collection<Uttils> result;

      result = uttilsRepository.findAll();
      Assert.notNull(result);

      return result;
   }

   public Uttils findOne(int uttilsId) {
      Assert.isTrue(uttilsId != 0);
      Uttils result;
      result = uttilsRepository.findOne(uttilsId);
      Assert.notNull(result);

      return result;
   }

   public Uttils save(Uttils uttils) {
      Assert.notNull(uttils);

      Uttils result;

      result = uttilsRepository.save(uttils);

      return result;
   }

   public void delete(Uttils uttils) {
      Assert.notNull(uttils);
      Assert.isTrue(uttils.getId() != 0);
      Assert.isTrue(uttilsRepository.exists(uttils.getId()));

      uttilsRepository.delete(uttils);
   }

   // Other business methods -------------------------------------------------


}