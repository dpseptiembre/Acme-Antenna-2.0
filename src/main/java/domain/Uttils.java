package domain;

import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.SafeHtml;
import org.hibernate.validator.constraints.URL;

import javax.persistence.*;


@Entity
@Access(AccessType.PROPERTY)
public class Uttils extends DomainEntity {

	public Uttils() {
		super();
	}

	private String welcomeMezzage;
	private String logo;
	private String systemName;
	private String welcomeMezzageES;

	@NotBlank
	@SafeHtml(whitelistType = SafeHtml.WhiteListType.NONE)
	public String getWelcomeMezzage() {
		return welcomeMezzage;
	}

	public void setWelcomeMezzage(String welcomeMezzage) {
		this.welcomeMezzage = welcomeMezzage;
	}


	@NotBlank
	@SafeHtml(whitelistType = SafeHtml.WhiteListType.NONE)
	public String getSystemName() {
		return systemName;
	}

	public void setSystemName(String systemName) {
		this.systemName = systemName;
	}

	@NotBlank
	@SafeHtml(whitelistType = SafeHtml.WhiteListType.NONE)
	public String getWelcomeMezzageES() {
		return welcomeMezzageES;
	}

	public void setWelcomeMezzageES(String welcomeMezzageES) {
		this.welcomeMezzageES = welcomeMezzageES;
	}

	@NotBlank
	@URL
	public String getLogo() {
		return logo;
	}

	public void setLogo(String logo) {
		this.logo = logo;
	}


}
