
package domain;

import java.util.Date;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.Valid;

import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.SafeHtml;

@Entity
@Access(AccessType.PROPERTY)
public class Request extends DomainEntity {

	public Request() {
		super();
	}


	private CreditCard	creditCard;
	private Date		moment;
	private String		description;
	private Date		serviceDone;
	private String		descriptionResult;
	private Boolean		isAttended;

	private Handyworker	handyworker;
	private User		user;
	private Antenna		antenna;


	@Valid
	public CreditCard getCreditCard() {
		return this.creditCard;
	}

	public void setCreditCard(final CreditCard creditCard) {
		this.creditCard = creditCard;
	}

	@Temporal(TemporalType.TIMESTAMP)
	public Date getMoment() {
		return this.moment;
	}

	public void setMoment(final Date moment) {
		this.moment = moment;
	}

	@NotBlank
	@SafeHtml(whitelistType = SafeHtml.WhiteListType.NONE)
	public String getDescription() {
		return this.description;
	}

	public void setDescription(final String description) {
		this.description = description;
	}

	@Temporal(TemporalType.TIMESTAMP)
	public Date getServiceDone() {
		return this.serviceDone;
	}

	public void setServiceDone(final Date serviceDone) {
		this.serviceDone = serviceDone;
	}
	@SafeHtml(whitelistType = SafeHtml.WhiteListType.NONE)
	public String getDescriptionResult() {
		return this.descriptionResult;
	}

	public void setDescriptionResult(final String descriptionResult) {
		this.descriptionResult = descriptionResult;
	}

	public Boolean getIsAttended() {
		return this.isAttended;
	}

	public void setIsAttended(final Boolean isAttended) {
		this.isAttended = isAttended;
	}

	@ManyToOne
	public Handyworker getHandyworker() {
		return this.handyworker;
	}

	public void setHandyworker(final Handyworker handyworker) {
		this.handyworker = handyworker;
	}

	@ManyToOne
	public User getUser() {
		return this.user;
	}

	public void setUser(final User user) {
		this.user = user;
	}

	@OneToOne
	public Antenna getAntenna() {
		return this.antenna;
	}

	public void setAntenna(final Antenna antenna) {
		this.antenna = antenna;
	}

}
