
package domain;

import java.util.Collection;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Entity;
import javax.persistence.OneToMany;

import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.SafeHtml;

@Entity
@Access(AccessType.PROPERTY)
public class Handyworker extends Actor {

	public Handyworker() {
		super();
	}


	private String					antennaModel;
	private Collection<Request>		requests;
	private Collection<Tutorial>	tutorials;
	private Collection<Comment>		comments;


	@NotBlank
	@SafeHtml(whitelistType = SafeHtml.WhiteListType.NONE)
	public String getAntennaModel() {
		return this.antennaModel;
	}

	public void setAntennaModel(final String antennaModel) {
		this.antennaModel = antennaModel;
	}

	@OneToMany(mappedBy = "handyworker")
	public Collection<Request> getRequests() {
		return this.requests;
	}

	public void setRequests(final Collection<Request> requests) {
		this.requests = requests;
	}

	@OneToMany
	public Collection<Comment> getComments() {
		return this.comments;
	}

	public void setComments(final Collection<Comment> comments) {
		this.comments = comments;
	}

	@OneToMany(mappedBy = "handyworker")
	public Collection<Tutorial> getTutorials() {
		return this.tutorials;
	}

	public void setTutorials(final Collection<Tutorial> tutorials) {
		this.tutorials = tutorials;
	}

}
