package domain;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Entity;

import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.SafeHtml;

@Entity
@Access(AccessType.PROPERTY)
public class TabooWord extends DomainEntity {

	// Constructor ----------------------------

	public TabooWord() {
		super();
	}

	// Attributes ----------------------------

	private String word;

	@NotBlank
	@SafeHtml
	public String getWord() {
		return this.word;
	}

	public void setWord(final String word) {
		this.word = word;
	}
}
