
package domain;

import java.util.Collection;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Entity;
import javax.persistence.OneToMany;

@Entity
@Access(AccessType.PROPERTY)
public class Agent extends Actor {

	public Agent() {
		super();
	}


	private Collection<Banner>		banners;
	private Collection<Tutorial>	tutorials;
	private Collection<Comment>		comments;


	@OneToMany(mappedBy = "agent")
	public Collection<Banner> getBanners() {
		return this.banners;
	}

	public void setBanners(final Collection<Banner> banners) {
		this.banners = banners;
	}

	@OneToMany(mappedBy = "agent")
	public Collection<Tutorial> getTutorials() {
		return this.tutorials;
	}

	public void setTutorials(final Collection<Tutorial> tutorials) {
		this.tutorials = tutorials;
	}

	@OneToMany
	public Collection<Comment> getComments() {
		return this.comments;
	}

	public void setComments(final Collection<Comment> comments) {
		this.comments = comments;
	}

}
