
package domain;


import java.util.Collection;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Entity;
import javax.persistence.OneToMany;


@Entity
@Access(AccessType.PROPERTY)
public class Administrator extends Actor {

	//Constructor
	public Administrator() {
		super();
	}
	
	
	private Collection<Tutorial>	tutorials;
	
	@OneToMany(mappedBy = "administrator")
	public Collection<Tutorial> getTutorials() {
		return this.tutorials;
	}

	public void setTutorials(final Collection<Tutorial> tutorials) {
		this.tutorials = tutorials;
	}

}
