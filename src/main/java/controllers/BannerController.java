
package controllers;

import java.util.Collection;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import services.AgentService;
import services.BannerService;
import domain.Agent;
import domain.Banner;

@Controller
@RequestMapping("/banner")
public class BannerController extends AbstractController {

	public BannerController() {
		super();
	}


	@Autowired
	private BannerService	bannerService;
	
	@Autowired
	private AgentService agentService;


	protected static ModelAndView createEditModelAndView(final Banner banner) {
		ModelAndView result;

		result = BannerController.createEditModelAndView(banner, null);

		return result;
	}

	protected static ModelAndView createEditModelAndView(final Banner banner, final String message) {
		ModelAndView result;

		result = new ModelAndView("banner/edit");
		result.addObject("banner", banner);
		result.addObject("message", message);

		return result;
	}

	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public ModelAndView bannersList() {

		ModelAndView result;
		Collection<Banner> banners;
		banners = this.bannerService.findAll();
		result = new ModelAndView("banner/list");
		result.addObject("banners", banners);
		result.addObject("requestURI", "banner/list.do");

		result.addObject("bannerX", bannerService.getRamdomBanner());

		return result;
	}
	
	@RequestMapping(value = "/myList", method = RequestMethod.GET)
	public ModelAndView bannersMyList() {

		ModelAndView result;
		Collection<Banner> banners;
		Agent agent;
		agent=agentService.findByPrincipal();
		banners = this.bannerService.findAgentBanner(agent.getId());
		result = new ModelAndView("banner/list");
		result.addObject("banners", banners);
		result.addObject("requestURI", "banner/list.do");

		result.addObject("bannerX", bannerService.getRamdomBanner());

		return result;
	}

	@RequestMapping(value = "/create", method = RequestMethod.GET)
	public ModelAndView create() {

		ModelAndView result;
		Agent agent;
		agent=agentService.findByPrincipal();
		
		final Banner banner = this.bannerService.create();
		banner.setAgent(agent);
		result = BannerController.createEditModelAndView(banner);

		result.addObject("bannerX", bannerService.getRamdomBanner());

		return result;
	}

	@RequestMapping(value = "/edit", method = RequestMethod.GET)
	public ModelAndView edit(@RequestParam final int bannerId) {
		ModelAndView result;
		Banner banner;

		banner = this.bannerService.findOne(bannerId);
		Assert.notNull(banner);
		Assert.isTrue(agentService.findByPrincipal().getBanners().contains(banner));
		result = BannerController.createEditModelAndView(banner);

		result.addObject("bannerX", bannerService.getRamdomBanner());

		return result;
	}

	@RequestMapping(value = "/edit", method = RequestMethod.POST, params = "save")
	public ModelAndView save(@Valid final Banner banner, final BindingResult binding) {
		ModelAndView result;
		if (binding.hasErrors())
			result = BannerController.createEditModelAndView(banner);
		else
			try {
				this.bannerService.save(banner);
				result = new ModelAndView("welcome/index");
			} catch (final Throwable oops) {
				result = BannerController.createEditModelAndView(banner, "banner.commit.error");
			}

		result.addObject("bannerX", bannerService.getRamdomBanner());

		return result;
	}

	@RequestMapping(value = "/edit", method = RequestMethod.POST, params = "delete")
	public ModelAndView delete(final Banner banner) {
		ModelAndView result;
		try {
			this.bannerService.delete(banner);
			result = new ModelAndView("redirect:list.do");
		} catch (final Throwable oops) {
			result = BannerController.createEditModelAndView(banner, "banner.commit.error");
		}

		result.addObject("bannerX", bannerService.getRamdomBanner());

		return result;
	}
	
	@RequestMapping(value = "/delete", method = RequestMethod.GET)
	public ModelAndView bannerDelete(@RequestParam final int bannerId) {

		ModelAndView result;
		Banner banner;
		
		banner=bannerService.findOne(bannerId);
		Assert.isTrue(agentService.findByPrincipal().getBanners().contains(banner));
		bannerService.delete(banner);
		
		Collection<Banner> banners;
		banners = this.bannerService.findAll();
		result = new ModelAndView("banner/list");
		result.addObject("banners", banners);
		result.addObject("requestURI", "banner/list.do");

		result.addObject("bannerX", bannerService.getRamdomBanner());

		return result;
	}
}
