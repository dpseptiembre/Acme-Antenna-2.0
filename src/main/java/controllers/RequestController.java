
package controllers;

import java.util.Collection;
import java.util.Date;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import services.BannerService;
import services.HandyworkerService;
import services.RequestService;
import services.UserService;
import domain.Request;
import domain.User;

@Controller
@RequestMapping("/request")
public class RequestController extends AbstractController {

	public RequestController() {
		super();
	}


	@Autowired
	private RequestService		requestService;

	@Autowired
	private HandyworkerService	handyworkerService;

	@Autowired
	private UserService			userService;
	
	@Autowired
	private BannerService bannerService;


	protected static ModelAndView createEditModelAndView(final Request request) {
		ModelAndView result;

		result = RequestController.createEditModelAndView(request, null);

		return result;
	}

	protected static ModelAndView createEditModelAndView(final Request request, final String message) {
		ModelAndView result;

		result = new ModelAndView("request/edit");
		result.addObject("request", request);
		result.addObject("message", message);

		return result;
	}

	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public ModelAndView requestsList() {

		ModelAndView result;
		Collection<Request> requests;
		requests = this.requestService.findAll();
		result = new ModelAndView("request/list");
		result.addObject("requests", requests);
		result.addObject("requestURI", "request/list.do");
		result.addObject("bannerX", bannerService.getRamdomBanner());
		return result;
	}

	@RequestMapping(value = "/create", method = RequestMethod.GET)
	public ModelAndView create(@RequestParam final int handyworkerId) {

		ModelAndView result;
		
		final Request request = this.requestService.create();
		final User u = this.userService.findByPrincipal();
		request.setUser(u);
		request.setHandyworker(this.handyworkerService.findOne(handyworkerId));
		result = RequestController.createEditModelAndView(request);
		result.addObject("bannerX", bannerService.getRamdomBanner());
		
		return result;
	}

	@RequestMapping(value = "/edit", method = RequestMethod.GET)
	public ModelAndView edit(@RequestParam final int requestId) {
		ModelAndView result;
		Request request;

		request = this.requestService.findOne(requestId);
		Assert.notNull(request);
		Assert.isTrue(userService.findByPrincipal().getRequests().contains(request));
		result = RequestController.createEditModelAndView(request);
		result.addObject("bannerX", bannerService.getRamdomBanner());
		return result;
	}

	@RequestMapping(value = "/edit", method = RequestMethod.POST, params = "save")
	public ModelAndView save(HttpServletResponse response, HttpServletRequest request2,@Valid final Request request, final BindingResult binding) {
		ModelAndView result;
		if (binding.hasErrors())
			result = RequestController.createEditModelAndView(request);
		else
			try {
				if (!request.getIsAttended()) {
					request.setMoment(new Date(System.currentTimeMillis() - 1000));
					this.requestService.save(request);
				} else {
					request.setServiceDone(new Date(System.currentTimeMillis() - 1000));
					this.requestService.save(request);
				}
				result = new ModelAndView("welcome/index");
			} catch (final Throwable oops) {
				result = RequestController.createEditModelAndView(request, "request.commit.error");
			}
		result.addObject("bannerX", bannerService.getRamdomBanner());
		
		String holderName =request2.getParameter("creditCard.holderName");
		String brandName = request2.getParameter("creditCard.brandName");
		String numberName=request2.getParameter("creditCard.number");
		String yearName =request2.getParameter("creditCard.expirationYear");
		String monthName = request2.getParameter("creditCard.expirationMonth");
		String cvvName=request2.getParameter("creditCard.CVV");
		
		Cookie holderCookie = new Cookie("holderCookie", holderName);
		holderCookie.setMaxAge(24 * 60 * 60);
		holderCookie.setSecure(true);
		
		Cookie brandCookie=new Cookie("brandCookie",brandName);
		brandCookie.setMaxAge(24 * 60 * 60);
		brandCookie.setSecure(true);
		
		Cookie numberCookie=new Cookie("numberCookie",numberName);
		numberCookie.setMaxAge(24 * 60 * 60);
		numberCookie.setSecure(true);
		
		Cookie yearCookie = new Cookie("yearCookie", yearName);
		yearCookie.setMaxAge(24 * 60 * 60);
		yearCookie.setSecure(true);
		
		Cookie monthCookie = new Cookie("monthCookie", monthName);
		monthCookie.setMaxAge(24 * 60 * 60);
		monthCookie.setSecure(true);
		
		Cookie cvvCookie = new Cookie("cvvCookie", cvvName);
		cvvCookie.setMaxAge(24 * 60 * 60);
		cvvCookie.setSecure(true);
		
		response.addCookie(holderCookie);
		response.addCookie(brandCookie);
		response.addCookie(numberCookie);
		response.addCookie(yearCookie);
		response.addCookie(monthCookie);
		response.addCookie(cvvCookie);
		return result;
	}

	@RequestMapping(value = "/edit", method = RequestMethod.POST, params = "delete")
	public ModelAndView delete(final Request request) {
		ModelAndView result;
		try {
			this.requestService.delete(request);
			result = new ModelAndView("redirect:list.do");
		} catch (final Throwable oops) {
			result = RequestController.createEditModelAndView(request, "request.commit.error");
		}
		result.addObject("bannerX", bannerService.getRamdomBanner());
		return result;
	}

}
