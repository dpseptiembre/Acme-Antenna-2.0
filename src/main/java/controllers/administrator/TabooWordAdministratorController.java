package controllers.administrator;

import java.util.Collection;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import services.BannerService;
import services.TabooWordService;


import controllers.AbstractController;
import domain.TabooWord;


@Controller
@RequestMapping("/tabooWord/administrator")
public class TabooWordAdministratorController extends AbstractController {

	// Services ----------------------------------------------------------------

	@Autowired
	private TabooWordService tabooWordService;
	@Autowired
	private BannerService bannerService;

	// Constructors----------------------------------------------

	public TabooWordAdministratorController() {
		super();
	}

	protected ModelAndView createEditModelAndView(final TabooWord tabooWord) {
		ModelAndView result;

		result = createEditModelAndView(tabooWord, null);

		return result;
	}

	protected ModelAndView createEditModelAndView(final TabooWord tabooWord,
			final String message) {
		ModelAndView result;

		result = new ModelAndView("tabooWord/edit");
		result.addObject("tabooWord", tabooWord);
		result.addObject("message", message);

		return result;
	}

	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public ModelAndView tabooWordsList() {

		ModelAndView result;
		Collection<TabooWord> tabooWords;
		tabooWords = tabooWordService.findAll();
		result = new ModelAndView("tabooWord/list");
		result.addObject("tabooWords", tabooWords);
		result.addObject("requestURI", "tabooWord/administrator/list.do");
		result.addObject("bannerX", bannerService.getRamdomBanner());
		return result;
	}

	@RequestMapping(value = "/create", method = RequestMethod.GET)
	public ModelAndView create() {

		ModelAndView result;

		final TabooWord tabooWord = this.tabooWordService.create();
		result = createEditModelAndView(tabooWord);
		result.addObject("bannerX", bannerService.getRamdomBanner());
		return result;
	}
	/**
	@RequestMapping(value = "/edit", method = RequestMethod.GET)
	public ModelAndView edit(@RequestParam final int tabooWordId) {
		ModelAndView result;
		TabooWord tabooWord;

		tabooWord = this.tabooWordService.findOne(tabooWordId);
		Assert.notNull(tabooWord);
		result = createEditModelAndView(tabooWord);

		return result;
	}
	**/

	@RequestMapping(value = "/edit", method = RequestMethod.POST, params = "save")
	public ModelAndView save(@Valid final TabooWord tabooWord,
			final BindingResult binding) {
		ModelAndView result;
		if (binding.hasErrors())
			result = createEditModelAndView(tabooWord);
		else
			try {
				this.tabooWordService.save(tabooWord);
				result = new ModelAndView("redirect:list.do");
			} catch (final Throwable oops) {
				result = createEditModelAndView(tabooWord,
						"tabooWord.commit.error");
			}
		result.addObject("bannerX", bannerService.getRamdomBanner());
		return result;
	}

	@RequestMapping(value = "/delete", method = RequestMethod.GET)
	public ModelAndView delete(@RequestParam final int tabooWordId) {
		ModelAndView result;

		try {
			final TabooWord tabooWord = this.tabooWordService
					.findOne(tabooWordId);
			this.tabooWordService.delete(tabooWord);
			result = new ModelAndView("redirect:list.do");
		} catch (final Throwable oops) {
			final TabooWord tabooWord = this.tabooWordService
					.findOne(tabooWordId);
			result = createEditModelAndView(tabooWord, "tabooWord.commit.error");
		}
		result.addObject("bannerX", bannerService.getRamdomBanner());
		return result;
	}

	@RequestMapping(value = "/edit", method = RequestMethod.POST, params = "delete")
	public ModelAndView delete(final TabooWord tabooWord) {
		ModelAndView result;
		try {
			this.tabooWordService.delete(tabooWord);
			result = new ModelAndView("redirect:list.do");
		} catch (final Throwable oops) {
			result = createEditModelAndView(tabooWord, "tabooWord.commit.error");
		}
		result.addObject("bannerX", bannerService.getRamdomBanner());
		return result;
	}
}
