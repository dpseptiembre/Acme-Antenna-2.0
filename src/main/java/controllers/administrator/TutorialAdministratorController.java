package controllers.administrator;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import services.BannerService;
import services.TutorialService;

import controllers.AbstractController;

import domain.Tutorial;

@Controller
@RequestMapping("/tutorial/administrator")
public class TutorialAdministratorController extends AbstractController {
	
	// Services ----------------------------------------------------------------

		@Autowired
		private TutorialService tutorialService;
		@Autowired
		private BannerService bannerService;
		

		// Constructors----------------------------------------------

		public TutorialAdministratorController() {
			super();
		}

		protected ModelAndView createEditModelAndView(final Tutorial tutorial) {
			ModelAndView result;

			result = createEditModelAndView(tutorial, null);

			return result;
		}

		protected ModelAndView createEditModelAndView(final Tutorial tutorial,
				final String message) {
			ModelAndView result;

			result = new ModelAndView("tutorial/edit");
			result.addObject("tutorial", tutorial);
			result.addObject("message", message);

			return result;
		}
		
		@RequestMapping(value = "/list", method = RequestMethod.GET)
		public ModelAndView tabooTutorialList() {

			ModelAndView result;
			Collection<Tutorial> tutorials;
			tutorials = tutorialService.findAllWithTabooWord();
			result = new ModelAndView("tutorial/tabooList");
			result.addObject("tutorials", tutorials);
			result.addObject("requestURI", "tutorial/administrator/list.do");
			result.addObject("bannerX", bannerService.getRamdomBanner());
			return result;
		}
		
		@RequestMapping(value = "/delete", method = RequestMethod.GET)
		public ModelAndView tabooTutorialDelete(@RequestParam final int tutorialId) {

			ModelAndView result;
			Collection<Tutorial> tutorials;
			tutorialService.deleteTutorialWithTabooWord(tutorialId);
			tutorials = tutorialService.findAllWithTabooWord();
			result = new ModelAndView("tutorial/tabooList");
			result.addObject("tutorials", tutorials);
			result.addObject("requestURI", "tutorial/administrator/list.do");
			result.addObject("bannerX", bannerService.getRamdomBanner());
			return result;
		}


}
