package controllers.administrator;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import services.BannerService;
import services.CommentService;
import controllers.AbstractController;
import domain.Comment;

@Controller
@RequestMapping("/comment/administrator")
public class CommentAdministratorController extends AbstractController {

	// Services ----------------------------------------------------------------

	@Autowired
	private CommentService commentService;
	@Autowired
	private BannerService bannerService;

	// Constructors----------------------------------------------

	public CommentAdministratorController() {
		super();
	}

	protected ModelAndView createEditModelAndView(final Comment comment) {
		ModelAndView result;

		result = createEditModelAndView(comment, null);

		return result;
	}

	protected ModelAndView createEditModelAndView(final Comment comment,
			final String message) {
		ModelAndView result;

		result = new ModelAndView("comment/edit");
		result.addObject("comment", comment);
		result.addObject("message", message);

		return result;
	}

	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public ModelAndView tabooCommentList() {

		ModelAndView result;
		Collection<Comment> comments;
		comments = commentService.findAllWithTabooWord();
		result = new ModelAndView("comment/tabooList");
		result.addObject("comments", comments);
		result.addObject("requestURI", "comment/administrator/list.do");
		result.addObject("bannerX", bannerService.getRamdomBanner());
		return result;
	}

	@RequestMapping(value = "/delete", method = RequestMethod.GET)
	public ModelAndView tabooCommentDelete(@RequestParam final int commentId) {

		ModelAndView result;
		Comment comment;
		comment = commentService.findOne(commentId);
		Collection<Comment> comments;
		commentService.delete2(comment);

		comments = commentService.findAllWithTabooWord();
		result = new ModelAndView("comment/tabooList");
		result.addObject("comments", comments);
		result.addObject("requestURI", "comment/administrator/list.do");
		result.addObject("bannerX", bannerService.getRamdomBanner());
		return result;
	}
}
