package controllers;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import services.BannerService;
import services.PlatformService;
import domain.Platform;
import domain.Satellite;

@Controller
@RequestMapping("/platform")
public class PlatformController extends AbstractController {

	public PlatformController() {
		super();
	}

	@Autowired
	private PlatformService platformService;
	@Autowired
	private BannerService bannerService;

	protected static ModelAndView createEditModelAndView(final Platform platform) {
		ModelAndView result;

		result = PlatformController.createEditModelAndView(platform, null);

		return result;
	}

	protected static ModelAndView createEditModelAndView(
			final Platform platform, final String message) {
		ModelAndView result;

		result = new ModelAndView("platform/edit");
		result.addObject("platform", platform);
		result.addObject("message", message);

		return result;
	}

	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public ModelAndView platformsList(Integer satelliteId) {
		ModelAndView result;
		Collection<Platform> platforms;
		if (satelliteId != null) {
			platforms = this.platformService.findBySatelliteId(satelliteId);
		} else {
			platforms = this.platformService.findAll();
		}
		result = new ModelAndView("platform/list");
		result.addObject("platforms", platforms);
		result.addObject("requestURI", "platform/list.do");
		result.addObject("bannerX", bannerService.getRamdomBanner());
		return result;
	}
	
	@RequestMapping(value = "/listKeyword1", method = RequestMethod.POST)
	public ModelAndView listKeyword1(@RequestParam String keyword) {

		ModelAndView result;
		Collection<Platform> platforms;
		try {
			platforms = platformService.findByKeyword(keyword);
			result = new ModelAndView("platform/list");
			result.addObject("requestURI", "platform/listKeyword.do");
			result.addObject("platforms", platforms);
		} catch (Throwable error) {
			result = new ModelAndView("redirect:list.do");
		}
		result.addObject("bannerX", bannerService.getRamdomBanner());
		return result;
	}

}
