
package controllers;

import java.util.Collection;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import security.UserAccountService;
import services.AgentService;
import services.BannerService;
import domain.Agent;

@Controller
@RequestMapping("/agent")
public class AgentController extends AbstractController {

	//Services ----------------------------------------------------------------

	@Autowired
	private AgentService		agentService;
	@Autowired
	private UserAccountService	userAccountService;
	@Autowired
	private BannerService		bannerService;


	//Constructors----------------------------------------------

	public AgentController() {
		super();
	}

	protected static ModelAndView createEditModelAndView(final Agent agent) {
		ModelAndView result;

		result = AgentController.createEditModelAndView(agent, null);

		return result;
	}

	//Create Method -----------------------------------------------------------

	protected static ModelAndView createEditModelAndView(final Agent agent, final String message) {
		ModelAndView result;

		result = new ModelAndView("agent/edit");
		result.addObject("agent", agent);
		result.addObject("message", message);

		return result;

	}

	protected static ModelAndView createEditModelAndView2(final Agent agent) {
		ModelAndView result;

		result = AgentController.createEditModelAndView2(agent, null);

		return result;
	}

	// Edition ---------------------------------------------------------

	protected static ModelAndView createEditModelAndView2(final Agent agent, final String message) {
		ModelAndView result;

		result = new ModelAndView("agent/register");
		result.addObject("agent", agent);
		result.addObject("message", message);

		return result;

	}

	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public ModelAndView agentList() {

		ModelAndView result;
		final Collection<Agent> agents = this.agentService.findAll();

		result = new ModelAndView("agent/list");
		result.addObject("agents", agents);
		result.addObject("requestURI", "agent/list.do");
		result.addObject("bannerX", bannerService.getRamdomBanner());

		return result;
	}

	@RequestMapping(value = "/create", method = RequestMethod.GET)
	public ModelAndView create() {

		ModelAndView result;
		Agent agent;

		agent = this.agentService.create();
		agent.setBan(false);
		result = AgentController.createEditModelAndView2(agent);
		result.addObject("bannerX", bannerService.getRamdomBanner());

		return result;

	}

	@RequestMapping(value = "/register", method = RequestMethod.POST, params = "save")
	public ModelAndView register(@Valid final Agent agent, final BindingResult binding) {
		ModelAndView result;
		if (binding.hasErrors())
			result = AgentController.createEditModelAndView2(agent, "general.commit.error2");
		else
			try {
				this.agentService.registerAsAgent(agent);
				result = new ModelAndView("agent/success");
			} catch (final Throwable oops) {
				result = AgentController.createEditModelAndView2(agent, "general.commit.error");
			}
		result.addObject("bannerX", bannerService.getRamdomBanner());
		return result;
	}

	@RequestMapping(value = "/edit", method = RequestMethod.GET)
	public ModelAndView edit(@RequestParam final int agentId) {
		ModelAndView result;
		Agent agent;
		agent = agentService.findByPrincipal();
		Assert.notNull(agent);
		result = AgentController.createEditModelAndView(agent);
		result.addObject("bannerX", bannerService.getRamdomBanner());
		return result;
	}

	@RequestMapping(value = "/edit", method = RequestMethod.POST, params = "save")
	public ModelAndView save(@Valid final Agent agent, final BindingResult binding) {
		ModelAndView result;
		if (binding.hasErrors())
			result = AgentController.createEditModelAndView(agent);
		else
			try {
				this.agentService.save(agent);
				result = new ModelAndView("agent/success");
			} catch (final Throwable oops) {
				result = AgentController.createEditModelAndView(agent, "agent.commit.error");
			}
		result.addObject("bannerX", bannerService.getRamdomBanner());
		return result;
	}

	@RequestMapping(value = "/edit", method = RequestMethod.POST, params = "delete")
	public ModelAndView delete(final Agent agent) {
		ModelAndView result;
		try {
			this.agentService.delete(agent);
			result = new ModelAndView("welcome/index");
		} catch (final Throwable oops) {
			result = AgentController.createEditModelAndView(agent, "agent.commit.error");
		}
		result.addObject("bannerX", bannerService.getRamdomBanner());
		return result;
	}

}
