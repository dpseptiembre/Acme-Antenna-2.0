/*
 * AdministratorController.java
 * 
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the
 * TDG Licence, a copy of which you may download from
 * http://www.tdg-seville.info/License.html
 */

package controllers;

import java.util.ArrayList;
import services.UttilsService;
import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import services.ActorService;
import services.AdministratorService;
import services.AgentService;
import services.BannerService;
import services.HandyworkerService;
import services.UserService;
import domain.Actor;
import domain.Agent;
import domain.Handyworker;
import domain.User;
import domain.Uttils;

@Controller
@RequestMapping("/administrator")
public class AdministratorController extends AbstractController {

	// Constructors -----------------------------------------------------------

	public AdministratorController() {
		super();
	}

	@Autowired
	private AdministratorService administratorService;

	@Autowired
	private AgentService agentService;

	@Autowired
	private HandyworkerService handyworkerService;

	@Autowired
	private UserService userService;

	@Autowired
	private ActorService actorService;
	
	@Autowired
	private UttilsService uttilsService;
	
	@Autowired
	private BannerService bannerService;

	// Action-1 ---------------------------------------------------------------

	@RequestMapping("/action-1")
	public ModelAndView action1() {
		ModelAndView result;

		result = new ModelAndView("administrator/action-1");

		return result;
	}

	// Action-2 ---------------------------------------------------------------

	@RequestMapping("/action-2")
	public ModelAndView action2() {
		ModelAndView result;

		result = new ModelAndView("administrator/action-2");

		return result;
	}

	@RequestMapping(value = "/dashboard", method = RequestMethod.GET)
	public ModelAndView dashboard() {

		ModelAndView result;

		try {
			final Double avgAntennaPerUser = this.administratorService
					.avgAntennaPerUser();
			final Double standarDesviationOfAntennasPerUser = this.administratorService
					.standarDesviationOfAntennasPerUser();
			final Double averageQualityOfAntennas = this.administratorService
					.averageQualityOfAntennas();
			final Double standarDesviationQualityOfAntennas = this.administratorService
					.standarDesviationQualityOfAntennas();
			final String dataobject = this.administratorService.createChart();
			final Collection<String> top3ModelsOfAntennas = this.administratorService
					.top3ModelsOfAntennas();
			final Double averageNumberOfTutorialsPerUser = this.administratorService
					.averageNumberOfTutorialsPerUser();
			final Double standarDesviationOfTutorialsPerUser = this.administratorService
					.standarDesviationOfTutorialsPerUser();
			final Double averageNumberOfCommentsPerTutorial = this.administratorService
					.averageNumberOfCommentsPerTutorial();
			final Double standarDesviationOfCommentsPerTutorial = this.administratorService
					.standarDesviationOfCommentsPerTutorial();
			final Collection<Actor> actorsPublishedTutorialAboveAVG = this.administratorService
					.actorsPublishedTutorialAboveAVG();
			final Double averageNumberOfRepliesPerComment = this.administratorService
					.averageNumberOfRepliesPerComment();
			final Double standarDesviationOfRepliesPerComment = this.administratorService
					.standarDesviationOfRepliesPerComment();
			final Double averageNumberOfLengthOfComment = this.administratorService
					.averageNumberOfLengthOfComment();
			final Double standarDesviationOfLengthOfComment = this.administratorService
					.standarDesviationOfLengthOfComment();
			final Double averageNumberOfPicturesPerTutorial = this.administratorService
					.averageNumberOfPicturesPerTutorial();
			final Double standarDesviationOfPicturesPeroTutorial = this.administratorService
					.standarDesviationOfPicturesPeroTutorial();
			final Double averageNumberOfPicturesPerComment = this.administratorService
					.averageNumberOfPicturesPerComment();
			final Double standarDesviationOfPicturesPeroComment = this.administratorService
					.standarDesviationOfPicturesPeroComment();
			final Double averageNumberOfRequestsPerUser = this.administratorService
					.averageNumberOfRequestsPerUser();
			final Double standarDesviationOfRequestsPerUser = this.administratorService
					.standarDesviationOfRequestsPerUser();
			final Double averageServicedRequestPerUser = this.administratorService
					.averageServicedRequestPerUser();
			final Double averageServicedRequestPerHandyworker = this.administratorService
					.averageServicedRequestPerHandyworker();
			final Double averageNumberOfBannersPerAgent = this.administratorService
					.averageNumberOfBannersPerAgent();
			final Collection<Agent> top3AgentsForBanners = this.administratorService
					.top3AgentsForBanners();

			result = new ModelAndView("administrator/dashboard");

			result.addObject("avgAntennaPerUser", avgAntennaPerUser);
			result.addObject("standarDesviationOfAntennasPerUser",
					standarDesviationOfAntennasPerUser);
			result.addObject("averageQualityOfAntennas",
					averageQualityOfAntennas);
			result.addObject("standarDesviationQualityOfAntennas",
					standarDesviationQualityOfAntennas);
			result.addObject("antennasPerModel", dataobject);
			result.addObject("top3ModelsOfAntennas", top3ModelsOfAntennas);
			result.addObject("averageNumberOfTutorialsPerUser",
					averageNumberOfTutorialsPerUser);
			result.addObject("standarDesviationOfTutorialsPerUser",
					standarDesviationOfTutorialsPerUser);
			result.addObject("averageNumberOfCommentsPerTutorial",
					averageNumberOfCommentsPerTutorial);
			result.addObject("standarDesviationOfCommentsPerTutorial",
					standarDesviationOfCommentsPerTutorial);
			result.addObject("actorsPublishedTutorialAboveAVG",
					actorsPublishedTutorialAboveAVG);
			result.addObject("averageNumberOfRepliesPerComment",
					averageNumberOfRepliesPerComment);
			result.addObject("standarDesviationOfRepliesPerComment",
					standarDesviationOfRepliesPerComment);
			result.addObject("averageNumberOfLengthOfComment",
					averageNumberOfLengthOfComment);
			result.addObject("standarDesviationOfLengthOfComment",
					standarDesviationOfLengthOfComment);
			result.addObject("averageNumberOfPicturesPerTutorial",
					averageNumberOfPicturesPerTutorial);
			result.addObject("standarDesviationOfPicturesPeroTutorial",
					standarDesviationOfPicturesPeroTutorial);
			result.addObject("averageNumberOfPicturesPerComment",
					averageNumberOfPicturesPerComment);
			result.addObject("standarDesviationOfPicturesPeroComment",
					standarDesviationOfPicturesPeroComment);
			result.addObject("averageNumberOfRequestsPerUser",
					averageNumberOfRequestsPerUser);
			result.addObject("standarDesviationOfRequestsPerUser",
					standarDesviationOfRequestsPerUser);
			result.addObject("averageServicedRequestPerUser",
					averageServicedRequestPerUser);
			result.addObject("averageServicedRequestPerHandyworker",
					averageServicedRequestPerHandyworker);
			result.addObject("averageNumberOfBannersPerAgent",
					averageNumberOfBannersPerAgent);
			result.addObject("top3AgentsForBanners", top3AgentsForBanners);
			

		} catch (final Exception e) {
			result = new ModelAndView("administrator/error");
			result.addObject("trace", e.getMessage());
		}

		result.addObject("bannerX", bannerService.getRamdomBanner());
		
		return result;

	}

	@RequestMapping(value = "/view", method = RequestMethod.GET)
	public ModelAndView view() {
		ModelAndView result;
		final Collection<User> users = this.userService.findAll();
		final Collection<Agent> agents = this.agentService.findAll();
		final Collection<Handyworker> handyworkers = this.handyworkerService
				.findAll();

		result = new ModelAndView("administrator/view");
		result.addObject("users", users);
		result.addObject("agents", agents);
		result.addObject("handyworkers", handyworkers);
		result.addObject("requestURI", "administrator/view.do");
		result.addObject("bannerX", bannerService.getRamdomBanner());

		return result;
	}

	@RequestMapping(value = "/unban", method = RequestMethod.GET)
	public ModelAndView unban(final int actorId) {

		ModelAndView result;
		final Actor actor = this.actorService.findOne(actorId);
		this.administratorService.unban(actor);

		final Collection<Actor> actors = this.actorService.findAllExceptAdmin();
		result = new ModelAndView("actor/list");
		result.addObject("actors", actors);
		result.addObject("requestURI", "administrator/actorList.do");
		result.addObject("bannerX", bannerService.getRamdomBanner());

		return result;

	}

	@RequestMapping(value = "/ban", method = RequestMethod.GET)
	public ModelAndView userBan(final int actorId) {

		ModelAndView result;
		final Actor actor = this.actorService.findOne(actorId);
		this.administratorService.ban(actor);
		final Collection<Actor> actors = this.actorService.findAllExceptAdmin();
		result = new ModelAndView("actor/list");
		result.addObject("actors", actors);
		result.addObject("requestURI", "administrator/actorList.do");
		result.addObject("bannerX", bannerService.getRamdomBanner());

		return result;
	}

	@RequestMapping(value = "/actorList", method = RequestMethod.GET)
	public ModelAndView actorList() {

		ModelAndView result;
		final Collection<Actor> actors = this.actorService.findAllExceptAdmin();

		result = new ModelAndView("actor/list");
		result.addObject("actors", actors);
		result.addObject("requestURI", "administrator/actorList.do");
		result.addObject("bannerX", bannerService.getRamdomBanner());

		return result;
	}

	@RequestMapping(value = "/utilsView", method = RequestMethod.GET)
	public ModelAndView utilsView() {

		ModelAndView result;
		ArrayList<Uttils> uttils = new ArrayList<>(uttilsService.findAll());
		Uttils uttils1 = uttils.get(0);

		result = new ModelAndView("administrator/utils");
		result.addObject("requestURI", "administrator/list.do");
		result.addObject("welcome", administratorService.getUttils()
				.getWelcomeMezzage());
		result.addObject("welcomeES", administratorService.getUttils()
				.getWelcomeMezzageES());
		result.addObject("systemName", administratorService.getUttils()
				.getSystemName());
		result.addObject("logo", bannerService.getRamdomBanner());
		result.addObject("bannerX", bannerService.getRamdomBanner());

		return result;
	}

	
    @RequestMapping(value = "/customize", method = RequestMethod.GET)
    public ModelAndView customize(@RequestParam String systemName, @RequestParam String logo, @RequestParam String welcome, @RequestParam String welcomeES){

        ModelAndView result;
        try {
            administratorService.customize(systemName,logo,welcome,welcomeES);
            result = new ModelAndView("welcome/index");
            result.addObject("banner", bannerService.getRamdomBanner());
            return result;
        } catch (Exception e) {
            result = new ModelAndView("administrator/error");
            result.addObject("trace", e.getMessage());
            result.addObject("bannerX", bannerService.getRamdomBanner());
            return result;
        }


    }
}
