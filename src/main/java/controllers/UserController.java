
package controllers;

import java.util.Collection;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import security.UserAccountService;
import services.BannerService;
import services.UserService;
import domain.User;

@Controller
@RequestMapping("/user")
public class UserController extends AbstractController {

	//Services ----------------------------------------------------------------

	@Autowired
	private UserService			userService;
	@Autowired
	private UserAccountService	userAccountService;
	@Autowired
	private BannerService bannerService;


	//Constructors----------------------------------------------

	public UserController() {
		super();
	}

	protected static ModelAndView createEditModelAndView(final User user) {
		ModelAndView result;

		result = UserController.createEditModelAndView(user, null);

		return result;
	}

	//Create Method -----------------------------------------------------------

	protected static ModelAndView createEditModelAndView(final User user, final String message) {
		ModelAndView result;

		result = new ModelAndView("user/edit");
		result.addObject("user", user);
		result.addObject("message", message);

		return result;

	}

	protected static ModelAndView createEditModelAndView2(final User user) {
		ModelAndView result;

		result = UserController.createEditModelAndView2(user, null);

		return result;
	}
	// Edition ---------------------------------------------------------

	protected static ModelAndView createEditModelAndView2(final User user, final String message) {
		ModelAndView result;

		result = new ModelAndView("user/register");
		result.addObject("user", user);
		result.addObject("message", message);

		return result;

	}

	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public ModelAndView userList() {

		ModelAndView result;
		final Collection<User> users = this.userService.findAll();

		result = new ModelAndView("user/list");
		result.addObject("users", users);
		result.addObject("requestURI", "user/list.do");
		result.addObject("bannerX", bannerService.getRamdomBanner());
		return result;
	}

	@RequestMapping(value = "/create", method = RequestMethod.GET)
	public ModelAndView create() {

		ModelAndView result;
		User user;

		user = this.userService.create();
		result = createEditModelAndView2(user);
		result.addObject("bannerX", bannerService.getRamdomBanner());
		return result;

	}

	@RequestMapping(value = "/register", method = RequestMethod.POST, params = "save")
	public ModelAndView register(@Valid User user,  BindingResult binding) {
		ModelAndView result;
		if (binding.hasErrors())
			result = createEditModelAndView2(user, "general.commit.error2");
		else
			try {
				this.userService.registerAsUser(user);
				result = new ModelAndView("user/success");
			} catch (Throwable oops) {
				result = createEditModelAndView2(user, "general.commit.error");
			}
		result.addObject("bannerX", bannerService.getRamdomBanner());
		return result;
	}

	@RequestMapping(value = "/edit", method = RequestMethod.GET)
	public ModelAndView edit(@RequestParam final int userId) {
		ModelAndView result;
		User user;
		user = userService.findByPrincipal();
		Assert.notNull(user);
		result = UserController.createEditModelAndView(user);
		result.addObject("bannerX", bannerService.getRamdomBanner());
		return result;
	}

	@RequestMapping(value = "/edit", method = RequestMethod.POST, params = "save")
	public ModelAndView save(@Valid final User user, final BindingResult binding) {
		ModelAndView result;
		if (binding.hasErrors())
			result = UserController.createEditModelAndView(user);
		else
			try {
				this.userService.save(user);
				result = new ModelAndView("user/success");
			} catch (final Throwable oops) {
				result = UserController.createEditModelAndView(user, "user.commit.error");
			}
		result.addObject("bannerX", bannerService.getRamdomBanner());
		return result;
	}

	@RequestMapping(value = "/edit", method = RequestMethod.POST, params = "delete")
	public ModelAndView delete(final User user) {
		ModelAndView result;
		try {
			this.userService.delete(user);
			result = new ModelAndView("welcome/index");
		} catch (final Throwable oops) {
			result = UserController.createEditModelAndView(user, "user.commit.error");
		}
		result.addObject("bannerX", bannerService.getRamdomBanner());
		return result;
	}

	@RequestMapping(value = "/view", method = RequestMethod.GET)
	public ModelAndView lessorViewAn(@RequestParam final int userId) {

		ModelAndView result;
		final User user = this.userService.findOne(userId);

		result = new ModelAndView("user/view");
		result.addObject("name", user.getName());
		result.addObject("surname", user.getSurname());
		result.addObject("email", user.getEmail());
		result.addObject("phoneNumber", user.getPhone());
		result.addObject("postalAddress", user.getPostalAddress());
		result.addObject("picture", user.getPicture());
		result.addObject("requestURI", "user/view.do");
		result.addObject("bannerX", bannerService.getRamdomBanner());
		return result;
	}
}
