
package controllers;

import java.util.Collection;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import security.UserAccountService;
import services.BannerService;
import services.HandyworkerService;
import domain.Handyworker;

@Controller
@RequestMapping("/handyworker")
public class HandyworkerController extends AbstractController {

	//Services ----------------------------------------------------------------

	@Autowired
	private HandyworkerService	handyworkerService;
	@Autowired
	private UserAccountService	userAccountService;
	@Autowired
	private BannerService bannerService;


	//Constructors----------------------------------------------

	public HandyworkerController() {
		super();
	}

	protected static ModelAndView createEditModelAndView(final Handyworker handyworker) {
		ModelAndView result;

		result = HandyworkerController.createEditModelAndView(handyworker, null);

		return result;
	}

	//Create Method -----------------------------------------------------------

	protected static ModelAndView createEditModelAndView(final Handyworker handyworker, final String message) {
		ModelAndView result;

		result = new ModelAndView("handyworker/edit");
		result.addObject("handyworker", handyworker);
		result.addObject("message", message);

		return result;

	}

	protected static ModelAndView createEditModelAndView2(final Handyworker handyworker) {
		ModelAndView result;

		result = HandyworkerController.createEditModelAndView2(handyworker, null);

		return result;
	}
	// Edition ---------------------------------------------------------

	protected static ModelAndView createEditModelAndView2(final Handyworker handyworker, final String message) {
		ModelAndView result;

		result = new ModelAndView("handyworker/register");
		result.addObject("handyworker", handyworker);
		result.addObject("message", message);

		return result;

	}

	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public ModelAndView handyworkerList() {

		ModelAndView result;
		final Collection<Handyworker> handyworkers = this.handyworkerService.findAll();

		result = new ModelAndView("handyworker/list");
		result.addObject("handyworkers", handyworkers);
		result.addObject("requestURI", "handyworker/list.do");
		result.addObject("bannerX", bannerService.getRamdomBanner());
		return result;
	}

	@RequestMapping(value = "/create", method = RequestMethod.GET)
	public ModelAndView create() {

		ModelAndView result;
		Handyworker handyworker;

		handyworker = this.handyworkerService.create();
		handyworker.setBan(false);
		result = HandyworkerController.createEditModelAndView2(handyworker);
		result.addObject("bannerX", bannerService.getRamdomBanner());
		return result;

	}

	@RequestMapping(value = "/register", method = RequestMethod.POST, params = "save")
	public ModelAndView register(@Valid final Handyworker handyworker, final BindingResult binding) {
		ModelAndView result;
		if (binding.hasErrors())
			result = HandyworkerController.createEditModelAndView2(handyworker, "general.commit.error2");
		else
			try {
				this.handyworkerService.registerAsHandyworker(handyworker);
				result = new ModelAndView("handyworker/success");
			} catch (final Throwable oops) {
				result = HandyworkerController.createEditModelAndView2(handyworker, "general.commit.error");
			}
		result.addObject("bannerX", bannerService.getRamdomBanner());
		return result;
	}

	@RequestMapping(value = "/edit", method = RequestMethod.GET)
	public ModelAndView edit(@RequestParam final int handyworkerId) {
		ModelAndView result;
		Handyworker handyworker;
		handyworker = handyworkerService.findByPrincipal();
		Assert.notNull(handyworker);
		result = HandyworkerController.createEditModelAndView(handyworker);
		result.addObject("bannerX", bannerService.getRamdomBanner());
		return result;
	}

	@RequestMapping(value = "/edit", method = RequestMethod.POST, params = "save")
	public ModelAndView save(@Valid final Handyworker handyworker, final BindingResult binding) {
		ModelAndView result;
		if (binding.hasErrors())
			result = HandyworkerController.createEditModelAndView(handyworker);
		else
			try {
				this.handyworkerService.save(handyworker);
				result = new ModelAndView("handyworker/success");
			} catch (final Throwable oops) {
				result = HandyworkerController.createEditModelAndView(handyworker, "handyworker.commit.error");
			}
		result.addObject("bannerX", bannerService.getRamdomBanner());
		return result;
	}

	@RequestMapping(value = "/edit", method = RequestMethod.POST, params = "delete")
	public ModelAndView delete(final Handyworker handyworker) {
		ModelAndView result;
		try {
			this.handyworkerService.delete(handyworker);
			result = new ModelAndView("welcome/index");
		} catch (final Throwable oops) {
			result = HandyworkerController.createEditModelAndView(handyworker, "handyworker.commit.error");
		}
		result.addObject("bannerX", bannerService.getRamdomBanner());
		return result;
	}

}
