
package controllers.handyworker;

import java.util.Collection;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import services.BannerService;
import services.HandyworkerService;
import services.RequestService;
import controllers.AbstractController;
import domain.Antenna;
import domain.Handyworker;
import domain.Request;

@Controller
@RequestMapping("/request/handyworker")
public class RequestHandyworkerController extends AbstractController {

	// Services ----------------------------------------------------------------

	@Autowired
	private RequestService		requestService;

	@Autowired
	private HandyworkerService	handyworkerService;
	@Autowired
	private BannerService bannerService;


	// Constructors----------------------------------------------

	public RequestHandyworkerController() {
		super();
	}

	protected ModelAndView createEditModelAndView(final Request request) {
		ModelAndView result;

		result = this.createEditModelAndView(request, null);

		return result;
	}

	protected ModelAndView createEditModelAndView(final Request request, final String message) {
		ModelAndView result;

		result = new ModelAndView("request/edit");
		result.addObject("request", request);
		result.addObject("message", message);

		return result;
	}

	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public ModelAndView requestsList() {

		ModelAndView result;
		Collection<Request> requests;
		Handyworker handyworker;
		handyworker = this.handyworkerService.findByPrincipal();
		requests = this.requestService.findByHandyworkerId(handyworker.getId());
		result = new ModelAndView("request/list");
		result.addObject("requests", requests);
		result.addObject("requestURI", "request/handyworker/list.do");
		result.addObject("bannerX", bannerService.getRamdomBanner());
		return result;
	}
	
	@RequestMapping(value = "/alreadyList", method = RequestMethod.GET)
	public ModelAndView requestsAlreadyList() {

		ModelAndView result;
		Collection<Request> requests;
		Handyworker handyworker;
		handyworker = this.handyworkerService.findByPrincipal();
		requests = this.requestService.findAlreadyRequestByHandyworkerId(handyworker.getId());
		result = new ModelAndView("request/list");
		result.addObject("requests", requests);
		result.addObject("requestURI", "request/handyworker/list.do");
		result.addObject("bannerX", bannerService.getRamdomBanner());
		return result;
	}
	
	@RequestMapping(value = "/noYetList", method = RequestMethod.GET)
	public ModelAndView requestsnoYetList() {

		ModelAndView result;
		Collection<Request> requests;
		Handyworker handyworker;
		handyworker = this.handyworkerService.findByPrincipal();
		requests = this.requestService.findNoYetRequestByHandyworkerId(handyworker.getId());
		result = new ModelAndView("request/list");
		result.addObject("requests", requests);
		result.addObject("requestURI", "request/handyworker/list.do");
		result.addObject("bannerX", bannerService.getRamdomBanner());
		return result;
	}
	
	@RequestMapping(value = "/serviceRequest", method = RequestMethod.GET)
	public ModelAndView serviceRequest(@Valid final int requestId) {

		ModelAndView result;
		Collection<Request> requests;
		Handyworker handyworker;
		handyworker = this.handyworkerService.findByPrincipal();
		Request request;
		request=requestService.findOne(requestId);
		Assert.isTrue(handyworker==request.getHandyworker(), "request.principal.error");
		requestService.serviceRequest(request);
		requests = this.requestService.findAlreadyRequestByHandyworkerId(handyworker.getId());
		result = new ModelAndView("request/list");
		result.addObject("requests", requests);
		result.addObject("requestURI", "request/handyworker/list.do");
		result.addObject("bannerX", bannerService.getRamdomBanner());
		return result;
	}
	
	@RequestMapping(value = "/serviceDone", method = RequestMethod.GET)
	public ModelAndView serviceDone(@Valid final int requestId) {

		ModelAndView result;
		Collection<Request> requests;
		Handyworker handyworker;
		handyworker = this.handyworkerService.findByPrincipal();
		Request request;
		request=requestService.findOne(requestId);
		Assert.isTrue(handyworker==request.getHandyworker(), "request.principal.error");
		requestService.serviceDone(request);
		requests = this.requestService.findAlreadyRequestByHandyworkerId(handyworker.getId());
		result = new ModelAndView("request/list");
		result.addObject("requests", requests);
		result.addObject("requestURI", "request/handyworker/list.do");
		result.addObject("bannerX", bannerService.getRamdomBanner());
		return result;
	}

}
