
package controllers.user;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import services.BannerService;
import services.RequestService;
import services.UserService;
import controllers.AbstractController;
import domain.Request;
import domain.User;

@Controller
@RequestMapping("/request/user")
public class RequestUserController extends AbstractController {

	// Services ----------------------------------------------------------------

	@Autowired
	private RequestService	requestService;

	@Autowired
	private UserService		userService;
	@Autowired 
	private BannerService bannerService;


	// Constructors----------------------------------------------

	public RequestUserController() {
		super();
	}

	protected ModelAndView createEditModelAndView(final Request request) {
		ModelAndView result;

		result = this.createEditModelAndView(request, null);

		return result;
	}

	protected ModelAndView createEditModelAndView(final Request request, final String message) {
		ModelAndView result;

		result = new ModelAndView("request/edit");
		result.addObject("request", request);
		result.addObject("message", message);

		return result;
	}

	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public ModelAndView requestsList() {

		ModelAndView result;
		Collection<Request> requests;
		User user;
		user = this.userService.findByPrincipal();
		requests = this.requestService.findByUserId(user.getId());
		result = new ModelAndView("request/list");
		result.addObject("requests", requests);
		result.addObject("requestURI", "request/user/list.do");
		result.addObject("bannerX", bannerService.getRamdomBanner());
		return result;
	}
	
	@RequestMapping(value = "/alreadyList", method = RequestMethod.GET)
	public ModelAndView requestsAlreadyList() {

		ModelAndView result;
		Collection<Request> requests;
		User user;
		user = this.userService.findByPrincipal();
		requests = this.requestService.findAlreadyRequestByUserId(user.getId());
		result = new ModelAndView("request/list");
		result.addObject("requests", requests);
		result.addObject("requestURI", "request/user/list.do");
		result.addObject("bannerX", bannerService.getRamdomBanner());
		return result;
	}
	
	@RequestMapping(value = "/noYetList", method = RequestMethod.GET)
	public ModelAndView requestsnoYetList() {

		ModelAndView result;
		Collection<Request> requests;
		User user;
		user = this.userService.findByPrincipal();
		requests = this.requestService.findNoYetRequestByUserId(user.getId());
		result = new ModelAndView("request/list");
		result.addObject("requests", requests);
		result.addObject("requestURI", "request/user/list.do");
		result.addObject("bannerX", bannerService.getRamdomBanner());
		return result;
	}

}
