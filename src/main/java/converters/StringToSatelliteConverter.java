package converters;

import domain.Satellite;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import repositories.SatelliteRepository;


@Component
@Transactional
public class StringToSatelliteConverter implements Converter<String, Satellite> {

    @Autowired
    SatelliteRepository satelliteRepository;

    @Override
    public Satellite convert(String text) {
        Satellite result;
        int id;

        try {
            if (StringUtils.isEmpty(text))
                result = null;
            else {
                id = Integer.valueOf(text);
                result = satelliteRepository.findOne(id);
            }
        } catch (Throwable oops) {
            throw new IllegalArgumentException(oops);
        }

        return result;

    }

}