package converters;

import domain.Antenna;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import repositories.AntennaRepository;


@Component
@Transactional
public class StringToAntennaConverter implements Converter<String, Antenna> {

    @Autowired
    AntennaRepository antennaRepository;

    @Override
    public Antenna convert(String text) {
        Antenna result;
        int id;

        try {
            if (StringUtils.isEmpty(text))
                result = null;
            else {
                id = Integer.valueOf(text);
                result = antennaRepository.findOne(id);
            }
        } catch (Throwable oops) {
            throw new IllegalArgumentException(oops);
        }

        return result;

    }

}
