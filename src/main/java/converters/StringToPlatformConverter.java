package converters;

import domain.Platform;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import repositories.PlatformRepository;


@Component
@Transactional
public class StringToPlatformConverter implements Converter<String, Platform> {

    @Autowired
    PlatformRepository platformRepository;

    @Override
    public Platform convert(String text) {
        Platform result;
        int id;

        try {
            if (StringUtils.isEmpty(text))
                result = null;
            else {
                id = Integer.valueOf(text);
                result = platformRepository.findOne(id);
            }
        } catch (Throwable oops) {
            throw new IllegalArgumentException(oops);
        }

        return result;

    }

}
