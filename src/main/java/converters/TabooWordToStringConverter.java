package converters;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import domain.TabooWord;

@Component
@Transactional
public class TabooWordToStringConverter implements Converter<TabooWord, String> {

    @Override
    public String convert(TabooWord tabooWord) {
        Assert.notNull(tabooWord);
        String result;
        result = String.valueOf(tabooWord.getId());
        return result;
    }
}