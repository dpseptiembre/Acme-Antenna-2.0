
package converters;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import domain.Handyworker;

@Component
@Transactional
public class HandyworkerToStringConverter implements Converter<Handyworker, String> {

	@Override
	public String convert(final Handyworker handyworker) {
		Assert.notNull(handyworker);
		String result;
		result = String.valueOf(handyworker.getId());
		return result;
	}

}
