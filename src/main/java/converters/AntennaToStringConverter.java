package converters;

import domain.Antenna;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

@Component
@Transactional
public class AntennaToStringConverter implements Converter<Antenna, String> {

    @Override
    public String convert(Antenna antenna) {
        Assert.notNull(antenna);
        String result;
        result = String.valueOf(antenna.getId());
        return result;
    }

}

