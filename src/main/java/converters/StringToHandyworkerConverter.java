
package converters;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;

import repositories.HandyworkerRepository;
import domain.Handyworker;

public class StringToHandyworkerConverter implements Converter<String, Handyworker> {

	@Autowired
	HandyworkerRepository	handyworkerRepository;


	@Override
	public Handyworker convert(final String text) {
		Handyworker result;
		int id;

		try {
			if (StringUtils.isEmpty(text))
				result = null;
			else {
				id = Integer.valueOf(text);
				result = this.handyworkerRepository.findOne(id);
			}
		} catch (final Throwable oops) {
			throw new IllegalArgumentException(oops);
		}

		return result;

	}

}
