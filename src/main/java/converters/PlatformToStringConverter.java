package converters;

import domain.Platform;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

@Component
@Transactional
public class PlatformToStringConverter implements Converter<Platform, String> {

    @Override
    public String convert(Platform platform) {
        Assert.notNull(platform);
        String result;
        result = String.valueOf(platform.getId());
        return result;
    }

}
