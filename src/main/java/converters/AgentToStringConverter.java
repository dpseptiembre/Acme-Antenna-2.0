
package converters;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import domain.Agent;

@Component
@Transactional
public class AgentToStringConverter implements Converter<Agent, String> {

	@Override
	public String convert(final Agent agent) {
		Assert.notNull(agent);
		String result;
		result = String.valueOf(agent.getId());
		return result;
	}

}
