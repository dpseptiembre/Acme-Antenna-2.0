
package repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import domain.Handyworker;

@Repository
public interface HandyworkerRepository extends JpaRepository<Handyworker, Integer> {

	@Query("select c from Handyworker c where c.userAccount.id = ?1")
	Handyworker findByUserAccountId(int userAccountId);

}
