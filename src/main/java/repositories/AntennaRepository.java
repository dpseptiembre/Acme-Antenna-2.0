
package repositories;

import java.util.Collection;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import domain.Antenna;

@Repository
public interface AntennaRepository extends JpaRepository<Antenna, Integer> {

	@Query("select a from Antenna a where a.user.id=?1")
	Collection<Antenna> findByUserId(int id);

}
