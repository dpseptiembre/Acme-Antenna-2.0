
package repositories;

import java.util.Collection;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import domain.Request;

@Repository
public interface RequestRepository extends JpaRepository<Request, Integer> {

	@Query("select r from Request r where r.antenna.id=?1")
	Collection<Request> findRequestAntenna(Integer antennaId);

	@Query("select r from Request r where r.user.id=?1")
	Collection<Request> findByUserId(int userId);

	@Query("select r from Request r where r.handyworker.id=?1")
	Collection<Request> findByHandyworkerId(int handyworkerId);
	
	@Query("select r from Request r where r.user.id=?1 and r.isAttended=true")
	Collection<Request> findAlreadyRequestByUserId(int userId);

	@Query("select r from Request r where r.user.id=?1 and r.isAttended=false")
	Collection<Request> findNoYetRequestByUserId(int userId);
	
	@Query("select r from Request r where r.handyworker.id=?1 and r.isAttended=true")
	Collection<Request> findAlreadyRequestByHandyworkerId(int userId);

	@Query("select r from Request r where r.handyworker.id=?1 and r.isAttended=false")
	Collection<Request> findNoYetRequestByHandyworkerId(int userId);
}
