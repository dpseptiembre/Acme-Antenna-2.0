
package repositories;

import java.util.Collection;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import domain.Actor;
import domain.Administrator;

@Repository
public interface AdministratorRepository extends JpaRepository<Administrator, Integer> {

	@Query("select avg(antennas.size) from User")
	Double averageNumberOfAntennasPerUser();
	@Query("select stddev(antennas.size) from User")
	Double standarDesviationOfAntennasPerUser();

	@Query("select avg(quality) from Antenna")
	Double averageQualityOfAntennas();
	@Query("select stddev(quality) from Antenna")
	Double standarDesviationQualityOfAntennas();

	@Query("select a.model from Antenna a")
	Collection<String> models();
	//ToDo A chart with the number of antennas per model.

	@Query("select a.model from Antenna a")
	Page<String> top3ModelsOfAntennas(Pageable pageable);

	//Para poder obtener el top 3 hay que usar el m�todo query.setMaxResults();

	@Query("select avg(tutorials.size) from User")
	Double averageNumberOfTutorialsPerUser();
	@Query("select stddev(tutorials.size) from User")
	Double standarDesviationOfTutorialsPerUser();
	@Query("select avg(comments.size) from Tutorial")
	Double averageNumberOfCommentsPerTutorial();
	@Query("select stddev(comments.size) from Tutorial")
	Double standarDesviationOfCommentsPerTutorial();

	//ToDo The actors who have published a number of tutorials that is above the average plus the standard deviation.
	@Query("select u from User u join u.tutorials ut group by u having count(ut) > ((select avg(tutorials.size) from User) + (select avg(tutorials.size) from Handyworker) + (select avg(tutorials.size) from Agent) + (select stddev(tutorials.size) from User) + (select stddev(tutorials.size) from Handyworker) + (select stddev(tutorials.size) from Agent)) UNION ALL select a from Agent a join a.tutorials at group by a having count(at) > ((select avg(tutorials.size) from User) + (select avg(tutorials.size) from Handyworker) + (select avg(tutorials.size) from Agent) + (select stddev(tutorials.size) from User) + (select stddev(tutorials.size) from Handyworker) + (select stddev(tutorials.size) from Agent)) UNION ALL select h from Handyworker h join h.tutorials ht group by h having count(ht) > ((select avg(tutorials.size) from User) + (select avg(tutorials.size) from Handyworker) + (select avg(tutorials.size) from Agent) + (select stddev(tutorials.size) from User) + (select stddev(tutorials.size) from Handyworker) + (select stddev(tutorials.size) from Agent))")
	Collection<Actor> actorsPublishedTutorialAboveAVG();

	//The average and the standard deviation of the number of replies per comment.
	@Query("select avg(replies.size) from Comment")
	Double averageNumberOfRepliesPerComment();
	@Query("select stddev(comments.size) from Tutorial")
	Double standarDesviationOfRepliesPerComment();

	//The average and the standard deviation of the length of the comments.
	@Query("select avg(LENGTH(c.text)) from Comment c")
	Double averageNumberOfLengthOfComment();
	@Query("select stddev(LENGTH(c.text)) from Comment c")
	Double standarDesviationOfLengthOfComment();

	@Query("select avg(pictures.size) from Tutorial")
	Double averageNumberOfPicturesPerTutorial();
	@Query("select stddev(pictures.size) from Tutorial")
	Double standarDesviationOfPicturesPeroTutorial();
	@Query("select avg(pictures.size) from Comment")
	Double averageNumberOfPicturesPerComment();
	@Query("select stddev(pictures.size) from Comment")
	Double standarDesviationOfPicturesPeroComment();

	@Query("select avg(requests.size) from User")
	Double averageNumberOfRequestsPerUser();
	@Query("select stddev(requests.size) from User")
	Double standarDesviationOfRequestsPerUser();
	@Query("select avg (u.requests.size) from User u join u.requests b where b.isAttended = true")
	Double averageServicedRequestPerUser();
	@Query("select avg (u.requests.size) from Handyworker u join u.requests b where b.isAttended = true")
	Double averageServicedRequestPerHandyworker();

	@Query("select avg(banners.size) from Agent")
	Double averageNumberOfBannersPerAgent();
	//ToDo The top-3 agents in terms of number of banners

}
