
package services;

import java.util.List;

import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.encoding.Md5PasswordEncoder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.util.Assert;

import security.UserAccount;
import security.UserAccountService;
import utilities.AbstractTest;
import domain.Handyworker;
import domain.Request;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
	"classpath:spring/datasource.xml", "classpath:spring/config/packages.xml"
})
@Transactional
public class HandyWorkerServiceTest extends AbstractTest {

	@Autowired
	private HandyworkerService	handyworkerService;
	@Autowired
	private UserAccountService	userAccountService;
	@Autowired
	private RequestService		requestService;


	@Test
	public void registerAsHandyworker() {
		final Handyworker h = this.handyworkerService.create();
		Assert.notNull(h);
		h.getUserAccount().setUsername("kkkkkk");
		h.setName("kkkkkk");
		h.setSurname("sdfsdf");
		h.setEmail("perri@gjail.com");
		Md5PasswordEncoder encoder;
		encoder = new Md5PasswordEncoder();
		final String hash = encoder.encodePassword("kkkkkk", null);
		h.getUserAccount().setPassword(hash);
		org.junit.Assert.assertNotNull(h);
	}

	@Test(expected = AssertionError.class)
	public void registerAsHAndyworkerNegative() {
		final Handyworker u = this.handyworkerService.create();
		u.setName("perrito");
		u.getUserAccount().setUsername("perrito");
		u.getUserAccount().setPassword("perrito");
		Assert.notNull(u);
		u.getUserAccount().setUsername(u.getUserAccount().getUsername());
		Md5PasswordEncoder encoder;
		encoder = new Md5PasswordEncoder();
		final String hash = encoder.encodePassword(u.getUserAccount().getPassword(), null);
		u.getUserAccount().setPassword(hash);
		final UserAccount userAccount = this.userAccountService.save(u.getUserAccount());
		u.setUserAccount(userAccount);
		Assert.notNull(u.getUserAccount().getAuthorities(), "authorities null al registrar");
		final Handyworker resu = this.handyworkerService.save(u);
		org.junit.Assert.assertNotNull(resu.getAntennaModel());
	}

	@Test
	public void serviceRequest() {
		this.authenticate("handyworker1");
		final List<Request> requests = (List<Request>) this.requestService.findAll();
		final Request request = requests.get(0);
		request.setIsAttended(true);
		final Request res = this.requestService.save(request);
		Assert.isTrue(res.getIsAttended());
		this.unauthenticate();
	}

	@Test(expected = IllegalArgumentException.class)
	public void serviceRequestNegative() {
		this.authenticate("handyworker1");
		final List<Request> requests = (List<Request>) this.requestService.findAll();
		final Request request = requests.get(0);
		request.setIsAttended(false);
		final Request res = this.requestService.save(request);
		Assert.isTrue(res.getIsAttended());
		this.unauthenticate();
	}

	@Test
	public void listServicedRequests() {
		this.authenticate("handyworker1");
		final Handyworker h = this.handyworkerService.findByPrincipal();
		final Request request = this.requestService.create();
		request.setIsAttended(true);
		final List<Request> requests = (List<Request>) this.requestService.findAlreadyRequestByHandyworkerId(h.getId());
		requests.add(request);
		Assert.notEmpty(requests);
		this.unauthenticate();
	}

	@Test(expected = IllegalArgumentException.class)
	public void listServicedRequestsNegative() {
		this.authenticate("handyworker1");
		final Handyworker h = this.handyworkerService.findByPrincipal();
		final List<Request> requests = (List<Request>) this.requestService.findAlreadyRequestByHandyworkerId(h.getId());
		Assert.notEmpty(requests);
		this.unauthenticate();
	}

	@Test
	public void listNotServicedRequests() {
		this.authenticate("handyworker1");
		final Handyworker h = this.handyworkerService.findByPrincipal();
		final List<Request> requests = (List<Request>) this.requestService.findNoYetRequestByHandyworkerId(h.getId());
		Assert.notEmpty(requests);
		this.unauthenticate();
	}

	@Test(expected = IndexOutOfBoundsException.class)
	public void listNotServicedRequestsNegative() {
		this.authenticate("handyworker1");
		final Handyworker h = this.handyworkerService.findByPrincipal();
		final List<Request> requests = (List<Request>) this.requestService.findNoYetRequestByHandyworkerId(h.getId());
		Assert.notNull(requests.get(80));
		this.unauthenticate();
	}

	@Test
	public void handyworkerList() {
		final List<Handyworker> handyworkers = (List<Handyworker>) this.handyworkerService.findAll();
		Assert.notNull(handyworkers);
	}

	@Test(expected = IndexOutOfBoundsException.class)
	public void handyworkerListNegative() {
		final List<Handyworker> handyworkers = (List<Handyworker>) this.handyworkerService.findAll();
		Assert.notNull(handyworkers.get(80));
	}

}
