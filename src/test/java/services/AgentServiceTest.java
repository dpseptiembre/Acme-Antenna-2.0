
package services;

import javax.transaction.Transactional;

import junit.framework.Assert;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.encoding.Md5PasswordEncoder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import security.UserAccount;
import security.UserAccountService;
import utilities.AbstractTest;
import domain.Actor;
import domain.Agent;
import domain.Banner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
	"classpath:spring/datasource.xml", "classpath:spring/config/packages.xml"
})
@Transactional
public class AgentServiceTest extends AbstractTest {

	@Autowired
	private AgentService		agentService;
	@Autowired
	private ActorService		actorService;
	@Autowired
	private UserAccountService	userAccountService;
	@Autowired
	private BannerService		bannerService;


	@Test
	public void registerAsAgent() {
		final Agent u = this.agentService.create();
		Assert.assertNotNull(u);
		u.getUserAccount().setUsername("kkkkkk");
		u.setName("kkkkkk");
		u.setSurname("sdfsdf");
		u.setEmail("perri@gjail.com");
		Md5PasswordEncoder encoder;
		encoder = new Md5PasswordEncoder();
		final String hash = encoder.encodePassword("kkkkkk", null);
		u.getUserAccount().setPassword(hash);
		org.junit.Assert.assertNotNull(u);
	}

	@Test(expected = javax.validation.ConstraintViolationException.class)
	public void registerAsAgentNegative() {
		final Agent u = this.agentService.create();
		u.setEmail("agent@hotmail.com");
		u.setName("agente");
		u.setPhone("987456321");
		u.setPostalAddress("C/ reina");
		u.setSurname("agentsur");
		final UserAccount userAccount = new UserAccount();
		userAccount.setUsername("generic");
		u.setUserAccount(userAccount);
		final Actor res = this.actorService.save(u);
		this.agentService.findUserAccount(u);
		Assert.assertNotNull(res);
	}

	@Test
	public void registerBanner() {
		this.authenticate("agent1");
		final Banner banner = this.bannerService.create();
		banner.setPicture("http://www.url.com");
		banner.setTargetPage("target");
		this.bannerService.save(banner);
		Assert.assertNotNull(banner);
		this.unauthenticate();
	}

	@Test(expected = AssertionError.class)
	public void registerBannerNegative() {
		this.authenticate("agent1");
		final Banner banner = this.bannerService.create();
		banner.setPicture("http://www.url.com");
		this.bannerService.save(banner);
		Assert.assertNotNull(banner.getTargetPage());
		this.unauthenticate();
	}

}
