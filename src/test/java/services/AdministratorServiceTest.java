
package services;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.NoSuchElementException;

import javax.transaction.Transactional;

import junit.framework.Assert;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import utilities.AbstractTest;
import domain.Banner;
import domain.Comment;
import domain.TabooWord;
import domain.Tutorial;
import domain.User;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
	"classpath:spring/datasource.xml", "classpath:spring/config/packages.xml"
})
@Transactional
public class AdministratorServiceTest extends AbstractTest {

	@Autowired
	private AdministratorService	administratorService;
	@Autowired
	private TabooWordService		tabooWordService;
	@Autowired
	private UserService				userService;
	@Autowired
	private BannerService			bannerService;
	@Autowired
	private TutorialService			tutorialService;
	@Autowired
	private CommentService			commentService;


	@Test
	public void deleteTutorial() {
		this.authenticate("administrator1");
		final List<Tutorial> tutorials = (List<Tutorial>) this.tutorialService.findAll();
		final Tutorial tutorial = tutorials.get(0);
		final int id = tutorial.getId();
		this.tutorialService.delete(tutorial);
		Assert.assertNull(this.tutorialService.findOne(id));
	}

	@Test(expected = IndexOutOfBoundsException.class)
	public void deleteTutorialNegative() {
		this.authenticate("administrator1");
		final List<Tutorial> tutorials = (List<Tutorial>) this.tutorialService.findAll();
		final Tutorial tutorial = tutorials.get(88);
		this.tutorialService.delete(tutorial);
	}

	@Test
	public void deleteComment() {
		this.authenticate("administrator1");
		final List<Comment> comments = (List<Comment>) this.commentService.findAll();
		final Comment comment = comments.get(0);
		final int id = comment.getId();
		this.commentService.delete(comment);
		Assert.assertNull(this.commentService.findOne(id));
	}

	@Test(expected = IndexOutOfBoundsException.class)
	public void deleteCommentNegative() {
		this.authenticate("administrator1");
		final List<Comment> comments = (List<Comment>) this.commentService.findAll();
		final Comment comment = comments.get(80);
		this.commentService.delete(comment);
	}

	@Test
	public void banUser() {
		this.authenticate("administrator1");
		final List<User> users = new ArrayList<>(this.userService.findAll());
		final User user = users.get(0);
		this.administratorService.ban(user);
		Assert.assertTrue(user.getBan());
		this.unauthenticate();
	}

	@Test(expected = IndexOutOfBoundsException.class)
	public void banUserNegative() {
		this.authenticate("administrator1");
		final List<User> users = new ArrayList<>(this.userService.findAll());
		final User user = users.get(80);
		this.administratorService.ban(user);
		Assert.assertTrue(user.getBan());
		this.unauthenticate();
	}

	@Test
	public void unBan() {
		this.authenticate("administrator1");
		final List<User> users = new ArrayList<>(this.userService.findAll());
		final User user = users.get(0);
		this.administratorService.unban(user);
		Assert.assertFalse(user.getBan());
		this.unauthenticate();
	}

	@Test(expected = IndexOutOfBoundsException.class)
	public void unBanNegative() {
		this.authenticate("administrator1");
		final List<User> users = new ArrayList<>(this.userService.findAll());
		final User user = users.get(80);
		this.administratorService.unban(user);
		Assert.assertFalse(user.getBan());
		this.unauthenticate();
	}

	@Test
	public void deleteBanner() {
		this.authenticate("administrator1");
		final List<Banner> banners = (List<Banner>) this.bannerService.findAll();
		final Banner banner = banners.get(0);
		final int id = banner.getId();
		this.bannerService.delete(banner);
		Assert.assertNull(this.bannerService.findOne(id));
	}

	@Test(expected = IndexOutOfBoundsException.class)
	public void deleteBannerNegative() {
		this.authenticate("administrator1");
		final List<Banner> banners = (List<Banner>) this.bannerService.findAll();
		final Banner banner = banners.get(80);
		final int id = banner.getId();
		this.bannerService.delete(banner);
		Assert.assertNull(this.bannerService.findOne(id));
	}

	@Test
	public void getTabooWords() {
		this.authenticate("administrator1");
		final List<TabooWord> tabooWords = (List<TabooWord>) this.tabooWordService.findAll();
		if (tabooWords.isEmpty())
			throw new NoSuchElementException("The taboo words list is empty");
		else {
			final TabooWord first = tabooWords.get(0);
			Assert.assertNotNull(first);
		}
	}

	@Test(expected = IndexOutOfBoundsException.class)
	public void getTabooWordsNegative() {
		this.authenticate("administrator1");
		final List<TabooWord> tabooWords = (List<TabooWord>) this.tabooWordService.findAll();
		final TabooWord first = tabooWords.get(80);
		Assert.assertNotNull(first);
	}

	@Test
	public void editTabooWord() {
		this.authenticate("administrator1");
		final TabooWord tabooWord = this.tabooWordService.create();
		Assert.assertNotNull(tabooWord);
		final TabooWord tabooWord2 = this.tabooWordService.save(tabooWord);
		Assert.assertNotNull(tabooWord2);
	}

	@Test
	public void deleteTabooWord() {
		this.authenticate("administrator1");
		final List<TabooWord> tabooWords = new ArrayList<>(this.tabooWordService.findAll());
		final Integer initialSize = tabooWords.size();
		tabooWords.remove(0);
		final Integer finalSize = tabooWords.size();
		org.junit.Assert.assertNotEquals(initialSize, finalSize);
		this.unauthenticate();
	}

	@Test
	public void tutorialTabooWords() {
		this.authenticate("administrator1");
		final HashSet<Tutorial> tutorials = (HashSet<Tutorial>) this.tutorialService.findAllWithTabooWord();
		if (tutorials.isEmpty())
			throw new NoSuchElementException("The tutorials list is empty");
		else
			for (final Tutorial first : tutorials) {
				Assert.assertNotNull(first);
				break;
			}
	}

	@Test(expected = IndexOutOfBoundsException.class)
	public void tutorialTabooWordsNegative() {
		this.authenticate("administrator1");
		final HashSet<Tutorial> tutorials = (HashSet<Tutorial>) this.tutorialService.findAllWithTabooWord();
		final Object[] res = tutorials.toArray();
		final Object x = res[80];
	}

	@Test(expected = NoSuchElementException.class)
	public void commentTabooWords() {
		this.authenticate("administrator1");
		final HashSet<Comment> comments = (HashSet<Comment>) this.commentService.findAllWithTabooWord();
		if (comments.isEmpty())
			throw new NoSuchElementException("The comments list is empty");
		else
			for (final Comment first : comments) {
				Assert.assertNotNull(first);
				break;
			}
	}

	@Test(expected = IndexOutOfBoundsException.class)
	public void commentTabooWordsNegative() {
		this.authenticate("administrator1");
		final HashSet<Comment> comments = (HashSet<Comment>) this.commentService.findAllWithTabooWord();
		final Object[] res = comments.toArray();
		final Object x = res[80];
	}

	@Test
	public void deleteTutorialTaboo() {
		this.authenticate("administrator1");
		final HashSet<Tutorial> tutorials = (HashSet<Tutorial>) this.tutorialService.findAllWithTabooWord();
		final Object[] res = tutorials.toArray();
		final Tutorial x = (Tutorial) res[0];
		final int id = x.getId();
		this.tutorialService.delete(x);
		Assert.assertNull(this.tutorialService.findOne(id));
	}

	@Test(expected = IndexOutOfBoundsException.class)
	public void deleteTutorialTabooNegative() {
		this.authenticate("administrator1");
		final HashSet<Tutorial> tutorials = (HashSet<Tutorial>) this.tutorialService.findAllWithTabooWord();
		final Object[] res = tutorials.toArray();
		final Tutorial x = (Tutorial) res[80];
		final int id = x.getId();
		this.tutorialService.delete(x);
		Assert.assertNull(this.tutorialService.findOne(id));
	}

	@Test(expected = ArrayIndexOutOfBoundsException.class)
	public void deleteCommentTaboo() {
		this.authenticate("administrator1");
		final HashSet<Comment> comments = (HashSet<Comment>) this.commentService.findAllWithTabooWord();
		final Object[] res = comments.toArray();
		final Comment x = (Comment) res[0];
		final int id = x.getId();
		this.commentService.delete(x);
		Assert.assertNull(this.commentService.findOne(id));
	}

	@Test(expected = IndexOutOfBoundsException.class)
	public void deleteCommentTabooNegative() {
		this.authenticate("administrator1");
		final HashSet<Comment> comments = (HashSet<Comment>) this.commentService.findAllWithTabooWord();
		final Object[] res = comments.toArray();
		final Comment x = (Comment) res[80];
		final int id = x.getId();
		this.commentService.delete(x);
		Assert.assertNull(this.commentService.findOne(id));
	}

}
